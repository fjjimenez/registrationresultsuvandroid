package mx.uv.registrationresultsuv.preferences;

import mx.uv.registrationresultsuv.util.C;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;
import org.androidannotations.annotations.sharedpreferences.SharedPref.Scope;

@SharedPref(Scope.UNIQUE)
public interface Preferences {

	@DefaultBoolean(true)
	boolean IsAnalitycsAccepted();

	@DefaultBoolean(false)
	boolean IsAnalitycsDialogAlreadyShowed();

	@DefaultBoolean(false)
	boolean SmallScreen();

	@DefaultString("")
	String DisplayDpi();

	@DefaultInt(0)
	int DensityDpi();

	@DefaultString(C.CACHE_SECTIONS_BY_DEFAULT)
	String LastUpdate();

	@DefaultInt(0)
	int DatabaseVersion();

}
