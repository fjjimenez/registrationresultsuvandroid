package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.ui.CellStep;
import mx.uv.registrationresultsuv.ui.CellStep_;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class StepAdapter extends ArrayAdapter<Step> {

	private final Activity mContext;
	private final ArrayList<Step> mSteps;

	public StepAdapter(Activity context, int textViewResourceId, ArrayList<Step> steps) {
		super(context, textViewResourceId, steps);
		mContext = context;
		mSteps = steps;
	}

	@Override
	public View getView(final int position, final View view, final ViewGroup parent) {

		CellStep rowMenu;
		if (view == null) {
			rowMenu = CellStep_.build(mContext);
		} else {
			rowMenu = (CellStep) view;
		}

		rowMenu.bind(getItem(position));

		return rowMenu;
	}

	@Override
	public int getCount() {
		return mSteps.size();
	}

	@Override
	public Step getItem(final int position) {
		return mSteps.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
