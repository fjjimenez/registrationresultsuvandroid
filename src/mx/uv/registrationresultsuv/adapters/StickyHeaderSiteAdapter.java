/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.ui.CellHeaderSite;
import mx.uv.registrationresultsuv.ui.CellHeaderSite_;
import mx.uv.registrationresultsuv.ui.CellSite;
import mx.uv.registrationresultsuv.ui.CellSite_;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.tonicartos.widget.stickygridheaders.StickyGridHeadersSimpleAdapter;

/**
 * 
 * @author frank
 */
public class StickyHeaderSiteAdapter extends BaseAdapter implements StickyGridHeadersSimpleAdapter {

	protected final LayoutInflater mInflater;
	protected final int mHeaderResId;
	protected final Context mContext;
	// protected final Activity mActivity;

	// protected int mOrderBy = 0;
	// protected boolean isScrolling;
	protected ArrayList<Site> mArtists;

	public StickyHeaderSiteAdapter(final Context context, final int itemResId, final int headerResId, final ArrayList<Site> artists) {
		// mActivity = activity;

		mContext = context;
		mHeaderResId = headerResId;
		mArtists = artists;
		mInflater = LayoutInflater.from(context);
		// mOrderBy = orderBy;
	}

	// public boolean isScrolling() {
	// return isScrolling;
	// }
	//
	// public void setScrolling(final boolean isScrolling) {
	// this.isScrolling = isScrolling;
	// }

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public int getCount() {
		return mArtists.size();
	}

	@Override
	public long getHeaderId(final int position) {
		// switch (mOrderBy) {
		// case 0:
		// // return this.getItem(position).getHeader();
		// default:
		// // return
		// // Integer.valueOf(this.getItem(position).getName().codePointAt(0));
		// }
		return Integer.valueOf(getItem(position).getCampus());
	}

	@Override
	public Site getItem(final int position) {
		return mArtists.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}

	@Override
	public View getHeaderView(final int position, final View convertView, final ViewGroup parent) {
		CellHeaderSite cellHeaderSite;
		if (convertView == null) {
			cellHeaderSite = CellHeaderSite_.build(mContext);
		} else {
			cellHeaderSite = (CellHeaderSite) convertView;
		}

		final Site site = getItem(position);
		// switch (mOrderBy) {
		// case 0:
		cellHeaderSite.bind(mContext.getString(site.getCampusResourceID()));
		// break;
		// default:
		// cellPublicationItem.bind(site.getName().substring(0, 1));
		// break;
		// }

		return cellHeaderSite;
	}

	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		CellSite cellSite;
		if (convertView == null) {
			cellSite = CellSite_.build(mContext);
		} else {
			cellSite = (CellSite) convertView;
		}

		cellSite.bind(getItem(position));

		return cellSite;
	}
}
