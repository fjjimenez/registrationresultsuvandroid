package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.ui.CellSite;
import mx.uv.registrationresultsuv.ui.CellSite_;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class SiteAdapter extends ArrayAdapter<Site> {

	private final Activity mContext;
	private final ArrayList<Site> mSites;

	public SiteAdapter(Activity context, int textViewResourceId, ArrayList<Site> sites) {
		super(context, textViewResourceId, sites);
		mContext = context;
		mSites = sites;
	}

	@Override
	public View getView(final int position, final View view, final ViewGroup parent) {

		CellSite rowMenu;
		if (view == null) {
			rowMenu = CellSite_.build(mContext);
		} else {
			rowMenu = (CellSite) view;
		}

		rowMenu.bind(getItem(position));

		return rowMenu;
	}

	@Override
	public int getCount() {
		return mSites.size();
	}

	@Override
	public Site getItem(final int position) {
		return mSites.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
