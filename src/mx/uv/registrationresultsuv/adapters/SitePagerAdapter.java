package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.ui.FrgSite;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SitePagerAdapter extends FragmentStatePagerAdapter {

	protected ArrayList<Site> sites;

	public SitePagerAdapter(final FragmentManager fm, ArrayList<Site> sites) {
		super(fm);
		this.sites = sites;
	}

	// @Override
	// public void destroyItem(final ViewGroup collection, final int position,
	// final Object object) {
	// final FragmentManager manager = ((Fragment) object).getFragmentManager();
	// final FragmentTransaction trans = manager.beginTransaction();
	// trans.remove((Fragment) object);
	// trans.commit();
	// }

	@Override
	public Fragment getItem(final int position) {
		return FrgSite.instance(sites.get(position));
	}

	// public static int getItemTitle(final int position) {
	// switch (position) {
	// case 0:
	// return R.string.title_most_popular;
	// case 1:
	// return R.string.title_best_rating;
	// case 2:
	// return R.string.title_last_added;
	// case 3:
	// return R.string.title_last_added;
	// default:
	// return R.string.title_default;
	// }
	// }
	//
	// public static int getItemTitle(final C.FEED_TYPE type) {
	// switch (type) {
	// case MOST_POPULAR:
	// return R.string.title_most_popular;
	// case BEST_RATING:
	// return R.string.title_best_rating;
	// case RECENTLY_ADDED:
	// return R.string.title_last_added;
	// case FAVORITED:
	// return R.string.title_favorited;
	// default:
	// return R.string.title_default;
	// }
	// }
	//
	// public static int getPosition(final C.FEED_TYPE type) {
	// switch (type) {
	// case MOST_POPULAR:
	// return 0;
	// case BEST_RATING:
	// return 1;
	// case RECENTLY_ADDED:
	// return 2;
	// default:
	// return 3;
	// }
	// }
	//
	// public static C.FEED_TYPE getType(final int currentItem) {
	// switch (currentItem) {
	// case 0:
	// return C.FEED_TYPE.MOST_POPULAR;
	// case 1:
	// return C.FEED_TYPE.BEST_RATING;
	// case 2:
	// return C.FEED_TYPE.RECENTLY_ADDED;
	// default:
	// return C.FEED_TYPE.FAVORITED;
	// }
	// }

	@Override
	public int getCount() {
		return sites.size();
	}
}
