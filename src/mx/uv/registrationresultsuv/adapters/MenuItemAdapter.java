package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.MenuDrawerItem;
import mx.uv.registrationresultsuv.ui.RowMenuItem;
import mx.uv.registrationresultsuv.ui.RowMenuItem_;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class MenuItemAdapter extends ArrayAdapter<MenuDrawerItem> {

	protected Context context;

	protected ArrayList<MenuDrawerItem> items;

	public MenuItemAdapter(final Context context, final int textViewResourceId, final ArrayList<MenuDrawerItem> items) {
		super(context, textViewResourceId, items);
		this.context = context;
		this.items = items;
	}

	@Override
	public View getView(final int position, final View view, final ViewGroup parent) {

		RowMenuItem rowMenu;
		if (view == null) {
			rowMenu = RowMenuItem_.build(context);
		} else {
			rowMenu = (RowMenuItem) view;
		}

		rowMenu.bind(getItem(position));

		return rowMenu;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public MenuDrawerItem getItem(final int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
