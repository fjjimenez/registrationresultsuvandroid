package mx.uv.registrationresultsuv.adapters;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.ui.FrgAbout;
import mx.uv.registrationresultsuv.ui.FrgRequestResults;
import mx.uv.registrationresultsuv.ui.FrgSettings;
import mx.uv.registrationresultsuv.ui.FrgSites;
import mx.uv.registrationresultsuv.ui.FrgSteps;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SectionPagerAdapter extends FragmentStatePagerAdapter {

	protected final static int NUM_PAGES = 5;

	public SectionPagerAdapter(final FragmentManager fm) {
		super(fm);
	}

	// @Override
	// public void destroyItem(final ViewGroup collection, final int position,
	// final Object object) {
	// final FragmentManager manager = ((Fragment) object).getFragmentManager();
	// final FragmentTransaction trans = manager.beginTransaction();
	// trans.remove((Fragment) object);
	// trans.commit();
	// }

	@Override
	public Fragment getItem(final int position) {
		switch (position) {
		case 0:
			return FrgSteps.instance();
		case 1:
			return FrgSites.instance();
		case 2:
			return FrgRequestResults.instance();
		case 3:
			return FrgSettings.instance();
		default:
			return FrgAbout.instance();
		}
	}

	public static int getItemTitle(final int position) {
		switch (position) {
		case 0:
			return R.string.tvw_summon;
		case 1:
			return R.string.tvw_hq;
		case 2:
			return R.string.tvw_results;
		case 3:
			return R.string.tvw_settings;
		default:
			return R.string.tvw_about_the_app;
		}
	}

	@Override
	public int getCount() {
		return NUM_PAGES;
	}
}
