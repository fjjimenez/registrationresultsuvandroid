package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Result;
import mx.uv.registrationresultsuv.ui.CellResult;
import mx.uv.registrationresultsuv.ui.CellResult_;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class ResultAdapter extends ArrayAdapter<Result> {

	private final Activity mContext;
	private final ArrayList<Result> mResults;

	public ResultAdapter(Activity context, int textViewResourceId, ArrayList<Result> results) {
		super(context, textViewResourceId, results);
		this.mContext = context;
		mResults = results;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		CellResult rowMenu;
		if (view == null) {
			rowMenu = CellResult_.build(mContext);
		} else {
			rowMenu = (CellResult) view;
		}

		rowMenu.bind(getItem(position));

		return rowMenu;
	}

	@Override
	public int getCount() {
		return mResults.size();
	}

	@Override
	public Result getItem(final int position) {
		return mResults.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
