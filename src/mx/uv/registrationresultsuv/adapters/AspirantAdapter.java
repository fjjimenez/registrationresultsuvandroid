package mx.uv.registrationresultsuv.adapters;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Aspirant;
import mx.uv.registrationresultsuv.ui.CellAspirant;
import mx.uv.registrationresultsuv.ui.CellAspirant_;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class AspirantAdapter extends ArrayAdapter<Aspirant> {

	private Activity mContext;
	private ArrayList<Aspirant> mAspirants;

	public AspirantAdapter(Activity context, int textViewResourceId, ArrayList<Aspirant> aspirants) {
		super(context, textViewResourceId, aspirants);
		this.mContext = context;
		this.mAspirants = aspirants;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {

		CellAspirant cellAspirant;
		if (view == null) {
			cellAspirant = CellAspirant_.build(mContext);
		} else {
			cellAspirant = (CellAspirant) view;
		}

		cellAspirant.bind(getItem(position));

		return cellAspirant;
	}

	@Override
	public int getCount() {
		return mAspirants.size();
	}

	@Override
	public Aspirant getItem(final int position) {
		return mAspirants.get(position);
	}

	@Override
	public long getItemId(final int position) {
		return position;
	}
}
