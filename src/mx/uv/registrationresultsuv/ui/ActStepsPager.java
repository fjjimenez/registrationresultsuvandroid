package mx.uv.registrationresultsuv.ui;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.StepPagerAdapter;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.OpenWebUrlEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.DatabaseUtil;
import mx.uv.registrationresultsuv.util.IntentUtil;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.mobandme.ada.exceptions.AdaFrameworkException;

@EActivity(R.layout.act_generic_pager)
@OptionsMenu(R.menu.act_steps_pager)
public class ActStepsPager extends SherlockFragmentActivity implements ActivityInterface {

	protected String mCaller = ActStepsPager.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected Handler mHandler;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.vwp_sections)
	protected ViewPager mVwpSection;

	protected StepPagerAdapter mAdapter;

	protected ActionBar mActionBar;

	protected MenuItem mMenuAddToCalendar;

	protected ArrayList<Step> mSteps;

	protected boolean isSmallScreen = false;

	@Extra
	protected int mIndex;

	private Step mStep;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mContext = getApplicationContext();
		isSmallScreen = UiUtil.isCloseToOneAspectRatio(mActivity);

		if (!isSmallScreen) {
			this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		}
		mActionBar = UiUtil.setupActionBarWithHome(this, isSmallScreen);

		mHandler = new Handler();
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {

		this.mMenuAddToCalendar = menu.findItem(R.id.ic_action_add_to_calendar);

		this.modifyActionBarButtonsVisibility();

		return true;
	}

	@OptionsItem
	protected void homeSelected() {
		finish();
	}

	@OptionsItem(R.id.ic_action_add_to_calendar)
	protected void icActionAddToCalendar() {
		if ((mSteps != null) && (mVwpSection != null)) {
			AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".addToCalendar", "Guardar en calendario " + mStep.getTitle());

			mStep = this.mSteps.get(mVwpSection.getCurrentItem());
			IntentUtil.addArtistToCalendar(mActivity, mStep);
		}

	}

	@AfterViews
	protected void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Paso");

		if ((mSteps == null) || (mSteps.isEmpty())) {
			getDataFromDB();
		}

	}

	@Background
	protected void getDataFromDB() {
		Persistence persistence = null;
		try {
			persistence = DatabaseUtil.getInstance(mContext, mCaller + ".setData()");
			mSteps = Persistence.getSteps(persistence);

			if ((mSteps != null) && (!mSteps.isEmpty())) {
				mAdapter = new StepPagerAdapter(getSupportFragmentManager(), mSteps);
			}
		} catch (AdaFrameworkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			persistence = null;
			DatabaseUtil.releaseDB(mCaller + ".setData()");
		}

		setDataFromDB();
	}

	@UiThread
	protected void setDataFromDB() {
		if (mAdapter != null) {
			mVwpSection.setAdapter(mAdapter);
			mVwpSection.setOnPageChangeListener(new SectionOnPageChangeListener());

			if (isSmallScreen) {
				this.changeTitle(mSteps.get(mIndex).getTitle());
			}

			mVwpSection.setCurrentItem(mIndex);

			modifyActionBarButtonsVisibility();
		}
	}

	public void onEvent(final OpenWebUrlEvent event) {
		IntentUtil.openWebUrl(mActivity, event.getUrl());
	}

	public void modifyActionBarButtonsVisibility() {
		if ((mSteps != null) && (mVwpSection != null)) {
			mStep = this.mSteps.get(mVwpSection.getCurrentItem());
			if ((mMenuAddToCalendar != null) && (mStep != null)) {
				if ((mStep.getFormatedDate() != null) && (mStep.getFormatedDate().length() > 0)) {
					this.mMenuAddToCalendar.setVisible(true);
				} else {
					this.mMenuAddToCalendar.setVisible(false);
				}
			} else if ((mMenuAddToCalendar != null) && (mStep == null)) {
				this.mMenuAddToCalendar.setVisible(false);
			}
		}
	}

	protected void changeTitle(String title) {
		if (mActionBar != null) {
			mActionBar.setTitle(title);
		}
	}

	@Override
	public boolean isSmallScreen() {
		return isSmallScreen;
	}

	protected class SectionOnPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int state) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int page) {
			modifyActionBarButtonsVisibility();
			if (isSmallScreen) {
				if ((mSteps != null) && (!mSteps.isEmpty())) {
					changeTitle(mSteps.get(page).getTitle());
				}
			}
		}
	}
}
