package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

@EViewGroup(R.layout.cell_site_header)
public class CellHeaderSite extends LinearLayout {

	@ViewById(R.id.tvw_header)
	protected TextView mTvwHeader;

	public CellHeaderSite(final Context context) {
		super(context);
	}

	public void bind(final String date) {
		mTvwHeader.setText(date);
	}
}