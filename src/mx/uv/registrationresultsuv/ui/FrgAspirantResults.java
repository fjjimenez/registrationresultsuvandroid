package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.ResultAdapter;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.OpenStepEvent;
import mx.uv.registrationresultsuv.events.ShareResultEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Aspirant;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.IntentUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_aspirant_results)
public class FrgAspirantResults extends SherlockFragment {

	protected String mCaller = FrgAspirantResults.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected ActivityInterface mCallback;
	protected Handler mHandler;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.tvw_loading)
	protected TextView mTvwLoading;

	@ViewById(R.id.tvw_name)
	protected TextView mTvwName;

	@ViewById(R.id.gvw_results)
	protected GridView mGvwResults;

	private ResultAdapter mResultAdapter;

	@FragmentArg
	protected Aspirant mAspirant;

	protected boolean actionPerformed = false;
	protected boolean isSmallScreen = false;

	public static FrgAspirantResults instance(Aspirant aspirant) {
		return FrgAspirantResults_.builder().mAspirant(aspirant).build();
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {

		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Resultado de aspirante");

		mHandler = new Handler();
		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);

		} else {
			mTvwTitle.setText(mAspirant.getName());
			Picasso.with(mContext).load(R.drawable.header_03_results).into(mIvwHeader);
		}

		mLltLoading.setVisibility(View.GONE);

		mResultAdapter = new ResultAdapter(this.mActivity, R.layout.cell_result, mAspirant.getResults());
		mGvwResults.setAdapter(mResultAdapter);
	}

	protected void onEvent(final OpenStepEvent event) {
		String detail = " corrimiento";
		if (event.getIndex() == 6) {
			detail = " inscripcion";
		}
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mBtnWhatsNext", "Ir a convocatoria," + detail);

		mHandler.post(new Runnable() {
			@Override
			public void run() {
				ActStepsPager_.intent(mActivity).mIndex(event.getIndex()).start();
			}
		});
	}

	protected void onEvent(final ShareResultEvent event) {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mBtnShare", "Compartir resultado");

		mHandler.post(new Runnable() {
			@Override
			public void run() {
				IntentUtil.shareString(mActivity, event.getResult().getShareResultString());
			}
		});
	}
}
