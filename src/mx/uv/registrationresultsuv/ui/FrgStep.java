package mx.uv.registrationresultsuv.ui;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.OpenWebUrlEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.DatabaseUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobandme.ada.exceptions.AdaFrameworkException;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_step)
public class FrgStep extends SherlockFragment {

	protected String mCaller = FrgStep.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected ActivityInterface mCallback;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.tvw_content)
	protected TextView mTvwContent;

	@ViewById(R.id.btn_action)
	protected Button mBtnAction;

	@FragmentArg
	protected Step mStep;

	protected ArrayList<mx.uv.registrationresultsuv.model.Button> mButtons;

	protected mx.uv.registrationresultsuv.model.Button mButton;

	protected boolean isSmallScreen = false;

	public static FrgStep instance(Step step) {
		return FrgStep_.builder().mStep(step).build();
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {

		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Paso");
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".setup", mStep.getTitle());

		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(mStep.getTitle());
			Picasso.with(mContext).load(mStep.getResourceId()).into(mIvwHeader);
		}

		mTvwContent.setText(mStep.getContent());

		mLltLoading.setVisibility(View.GONE);

		getDataFromDB();
	}

	@Background
	protected void getDataFromDB() {
		Persistence persistence = null;
		try {
			persistence = DatabaseUtil.getInstance(mContext, mCaller + ".getDataFromDB()");
			mButtons = Persistence.getButtons(persistence, mStep.getID());

			if ((mButtons != null) && (!mButtons.isEmpty())) {
				mButton = mButtons.get(0);
			}
		} catch (AdaFrameworkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			persistence = null;
			DatabaseUtil.releaseDB(mCaller + ".getDataFromDB()");
		}

		setDataFromDB();
	}

	@UiThread
	protected void setDataFromDB() {
		if (mButton != null) {
			mBtnAction.setText(mButton.getTitle());
			mBtnAction.setVisibility(View.VISIBLE);
		}
	}

	@Click(R.id.btn_action)
	protected void mButton() {
		if (mButton.getAction() != -1) {
			switch (mButton.getAction()) {
			case 1:
				// Move to sites
				ActSites_.intent(mActivity).start();
				break;
			case 2:
				// Move to results
				ActResults_.intent(mActivity).start();
				break;
			default:
				break;
			}
		} else if (!mButton.getUrl().equals("")) {
			AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mButton", mStep.getTitle() + " " + mButton.getTitle() + " " + mButton.getUrl());
			BusProvider.getInstance().post(new OpenWebUrlEvent(mButton.getUrl()));
		}
	}

}
