package mx.uv.registrationresultsuv.ui;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.StepAdapter;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.StepsDownloadedEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.DatabaseUtil;
import mx.uv.registrationresultsuv.util.NetworkCheckerUtil;
import mx.uv.registrationresultsuv.util.ToastUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobandme.ada.exceptions.AdaFrameworkException;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_generic_grid)
public class FrgSteps extends SherlockFragment {

	protected String mCaller = FrgSteps.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected Handler mHandler;
	protected ActivityInterface mCallback;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.gvw_items)
	protected GridView mGvwSteps;

	@ViewById(R.id.llt_no_internet)
	protected LinearLayout mLltNoInternet;

	@ViewById(R.id.ivw_empty)
	protected ImageView mIvwEmpty;

	protected StepAdapter mAdapter;

	protected ArrayList<Step> mSteps;

	protected boolean isVisibleToUser = false;
	protected boolean isNetworkAvailable = false;

	protected boolean isSmallScreen = false;

	public static FrgSteps instance() {
		return FrgSteps_.builder().build();
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		this.isVisibleToUser = isVisibleToUser;
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isNetworkAvailable = NetworkCheckerUtil.checkNetwork(mActivity);
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Convocatoria");

		mHandler = new Handler();

		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(R.string.tvw_summon);
			Picasso.with(mContext).load(R.drawable.header_01_steps).into(mIvwHeader);
		}

		getDataFromDB();
	}

	@Background
	public void getDataFromDB() {
		Persistence persistence = null;
		try {
			persistence = DatabaseUtil.getInstance(mContext, mCaller + ".getDataFromDB()");
			mSteps = Persistence.getSteps(mActivity, mPreferences, persistence, isNetworkAvailable);
			if ((mSteps != null) && (!mSteps.isEmpty())) {
				mAdapter = new StepAdapter(mActivity, R.layout.cell_step, mSteps);
			} else {
				if (!isNetworkAvailable) {
					showEmptyList();
				}
			}
		} catch (AdaFrameworkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			persistence = null;
			DatabaseUtil.releaseDB(mCaller + ".getDataFromDB()");
		}

		setDataFromDB();
	}

	@UiThread
	public void setDataFromDB() {
		if ((mSteps != null) && (!mSteps.isEmpty())) {
			mGvwSteps.setAdapter(mAdapter);
			mLltLoading.setVisibility(View.GONE);
		}
	}

	@ItemClick(R.id.gvw_items)
	protected void gvwItems(final int position) {
		if (mSteps != null) {
			ActStepsPager_.intent(mActivity).mIndex(position).start();
		}
	}

	public void onEvent(final StepsDownloadedEvent event) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if ((event.getSteps() != null) && (!event.getSteps().isEmpty())) {
					mSteps = event.getSteps();
					mAdapter = new StepAdapter(mActivity, R.layout.cell_step, mSteps);
				} else if ((mSteps == null) || (mSteps.isEmpty())) {
					showEmptyList();
				}
				setDataFromDB();
			}
		});
	}

	@UiThread
	protected void showEmptyList() {
		try {
			mLltLoading.setVisibility(View.GONE);
			mGvwSteps.setVisibility(View.GONE);
			mLltNoInternet.setVisibility(View.VISIBLE);
			Picasso.with(mContext).load(R.drawable.ic_empty).into(mIvwEmpty);
			if (isVisibleToUser) {
				ToastUtil.longToast(mActivity, R.string.ctn_service_unavailable);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
