package mx.uv.registrationresultsuv.ui;

import java.io.UnsupportedEncodingException;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.CenterAtLocationEvent;
import mx.uv.registrationresultsuv.interfaces.SlidingPanelActivityInterface;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.IntentUtil;
import mx.uv.registrationresultsuv.util.NetworkCheckerUtil;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.DrawableRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_site)
public class FrgSite extends SherlockFragment {

	protected final String mCaller = FrgSite.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected SlidingPanelActivityInterface mCallback;
	protected Handler mHandler;

	@Pref
	protected Preferences_ mPreferences;

	protected GoogleMap mMap;

	@ViewById(R.id.spl_root)
	protected SlidingUpPanelLayout mSplRoot;
	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.ivw_street_view)
	protected ImageView mIvwStreetView;

	@ViewById(R.id.ivw_drawer)
	protected ImageView mIvwDrawer;

	@ViewById(R.id.flt_drawer)
	protected FrameLayout mFltDrawer;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.tvw_zone)
	protected TextView mTvwZone;

	@ViewById(R.id.tvw_address)
	protected TextView mTvwAddress;

	@ViewById(R.id.tvw_phone)
	protected TextView mTvwPhone;

	@ViewById(R.id.flt_call)
	protected FrameLayout mBtnCall;

	@FragmentArg
	protected Site mSite;

	@DrawableRes(R.drawable.above_shadow)
	protected Drawable mDrwShadow;

	protected mx.uv.registrationresultsuv.model.Button mButton;

	protected LatLng mSiteLocation;

	protected LatLng mUserLocation;

	protected boolean isGooglePlayServicesAvailable = false;

	protected boolean userMarkerAdded = false;

	protected int orientation = -1;
	protected int width = -1;
	protected int height = -1;

	protected boolean isSmallScreen = false;

	public static FrgSite instance(Site site) {
		return FrgSite_.builder().mSite(site).build();
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);
		int googlePlayServicesAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
		if (googlePlayServicesAvailable == ConnectionResult.SUCCESS) {
			isGooglePlayServicesAvailable = true;
			getMap();
		} else {
			loadStaticImage();
		}
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (SlidingPanelActivityInterface) activity;
			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {

		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Sede");
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".setup", mSite.getName());

		orientation = UiUtil.getScreenOrientation(mActivity);

		Point size = UiUtil.getScreenSize(mActivity);

		width = size.x;
		height = size.y;

		mSiteLocation = new LatLng(mSite.getLatitude(), mSite.getLongitude());

		if ((android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) && (!isSmallScreen)) {
			mSplRoot.setAnchorPoint(0.4f);
		}
		mSplRoot.setDragView(mFltDrawer);
		mSplRoot.setPanelSlideListener(new SplRootPanelSlideListener());

		if (!NetworkCheckerUtil.checkNetwork(mActivity)) {
			Picasso.with(mContext).load(R.drawable.site_placeholder).into(mIvwStreetView);
			mSplRoot.showPane();
		}

		mTvwTitle.setText(mSite.getName());
		mTvwAddress.setText(mSite.getAddress());
		mTvwZone.setText(((Context) mActivity).getString(R.string.campus) + " " + mActivity.getString(mSite.getCampusResourceID()));
		mTvwPhone.setText(mSite.getPhoneText());

		mLltLoading.setVisibility(View.GONE);
	}

	@UiThread
	protected void getMap() {
		if ((mSite != null)) {
			FragmentManager fm = ((SherlockFragmentActivity) mActivity).getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();

			GoogleMapOptions options = new GoogleMapOptions();

			options.mapType(GoogleMap.MAP_TYPE_NORMAL).compassEnabled(true).rotateGesturesEnabled(true).tiltGesturesEnabled(true).zoomControlsEnabled(true).scrollGesturesEnabled(true);

			ft.replace(R.id.flt_map, SupportMapFragment.newInstance(options), String.valueOf(mSite.getID()));
			ft.commit();

			ft = null;
			fm = null;

			getFragment();
		}
	}

	@Background
	public void getFragment() {
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		FragmentManager fm = ((SherlockFragmentActivity) mActivity).getSupportFragmentManager();
		setupMap(fm.findFragmentByTag(String.valueOf(mSite.getID())));

	}

	@UiThread
	public void setupMap(Fragment frg) {

		if ((frg != null) && (mSite != null)) {

			mMap = ((SupportMapFragment) frg).getMap();

			if (mMap != null) {

				mIvwStreetView.setVisibility(View.GONE);

				mMap.setPadding(0, 0, 0, (int) (mIvwDrawer.getBottom() - mIvwDrawer.getTop() + mActivity.getResources().getDimension(R.dimen.tile_double_margin)));

				mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_site)).position(mSiteLocation).title(mSite.getName()));

				// Move the camera instantly to location with a zoom of 16.
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mSiteLocation, 16));

				// Zoom in, animating the camera.
				mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 2000, null);

			} else {
				loadStaticImage();
			}
		} else {
			loadStaticImage();
		}
	}

	@UiThread
	protected void loadStaticImage() {
		mIvwStreetView.setVisibility(View.VISIBLE);
		if (NetworkCheckerUtil.checkNetwork(mActivity)) {
			try {
				Picasso.with(mContext).load(IntentUtil.buildGoogleMapsImageAddress(mSite, width, height, orientation)).placeholder(R.drawable.site_placeholder).into(mIvwStreetView);

			} catch (UnsupportedEncodingException e) {
				Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
				// e.printStackTrace();
			}
		}

	}

	@Click(R.id.flt_call)
	protected void mCallButton() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mCallButton", "Llamar a sede " + mSite.getName() + " " + mSite.getCampusName(mActivity));
		IntentUtil.callPhone(mActivity, mSite.getName(), mSite.getPhone());
	}

	public void onEvent(CenterAtLocationEvent event) {
		if ((mMap != null) && (isGooglePlayServicesAvailable)) {
			switch (event.getType()) {
			case SITE:
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(event.getLocation(), 17));
				break;
			default:
				if (!userMarkerAdded) {
					mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_user)).position(event.getLocation()).title(mActivity.getString(R.string.mkr_my_location)));
					userMarkerAdded = true;
				}
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(event.getLocation(), 17));
				break;
			}
		}
	}

	public class SplRootPanelSlideListener implements PanelSlideListener {

		@Override
		public void onPanelSlide(View panel, float slideOffset) {
		}

		@Override
		public void onPanelCollapsed(View panel) {
			mCallback.setActionBarVisibility(true);
		}

		@Override
		public void onPanelExpanded(View panel) {
			mCallback.setActionBarVisibility(false);
		}

		@Override
		public void onPanelAnchored(View panel) {
			mCallback.setActionBarVisibility(true);
		}
	}
}
