package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.model.MenuDrawerItem;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

@EViewGroup(R.layout.row_menu_item)
public class RowMenuItem extends LinearLayout {

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	public RowMenuItem(final Context context) {
		super(context);
	}

	public void bind(final MenuDrawerItem item) {
		mTvwTitle.setText(item.getTitle());
	}
}