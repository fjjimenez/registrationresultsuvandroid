/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.model.Step;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * 
 * @author frank
 */
@EViewGroup(R.layout.cell_step)
public class CellStep extends RelativeLayout {

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.tvw_sub_title)
	protected TextView mTvwSubTitle;

	@ViewById(R.id.tvw_date)
	protected TextView mTvwDate;

	@ViewById(R.id.ivw_icon)
	protected ImageView mIvwIcon;

	public CellStep(Context context) {
		super(context);
	}

	public void bind(final Step item) {
		mTvwTitle.setText(item.getTitle());
		mTvwSubTitle.setText(item.getSubtitle());
		mTvwDate.setText(item.getFormatedDate());

		Picasso.with(getContext()).load(item.getResourceId()).into(mIvwIcon);
	}

}
