package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.SitePagerAdapter;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;

@EActivity(R.layout.act_generic_fragment)
public class ActResults extends SherlockFragmentActivity implements ActivityInterface {

	protected String mCaller = ActResults.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.vwp_sections)
	protected ViewPager mVwpSection;

	protected SitePagerAdapter mAdapter;

	protected ActionBar mActionBar;

	protected boolean isSmallScreen = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mContext = getApplicationContext();
		isSmallScreen = UiUtil.isCloseToOneAspectRatio(mActivity);

		if (!isSmallScreen) {
			this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		}
		mActionBar = UiUtil.setupActionBarWithHome(this, isSmallScreen);

	}

	@OptionsItem
	protected void homeSelected() {
		finish();
	}

	@AfterViews
	protected void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Resultados");

		if (isSmallScreen) {
			changeTitle(R.string.tvw_results);
		}

		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		final FrgRequestResults frgRequestResults = FrgRequestResults.instance();
		ft.replace(R.id.flt_content, frgRequestResults);
		ft.commit();

	}

	protected void changeTitle(int resourceId) {
		if (mActionBar != null) {
			mActionBar.setTitle(resourceId);
		}
	}

	@Override
	public boolean isSmallScreen() {
		return isSmallScreen;
	}
}
