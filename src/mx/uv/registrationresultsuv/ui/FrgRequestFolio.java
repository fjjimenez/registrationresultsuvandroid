package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.FolioRequestEvent;
import mx.uv.registrationresultsuv.events.ShowMessageEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Request;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.NetworkCheckerUtil;
import mx.uv.registrationresultsuv.util.ToastUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.SherlockFragment;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_request_folio)
public class FrgRequestFolio extends SherlockFragment {

	protected String mCaller = FrgRequestFolio.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected ActivityInterface mCallback;
	protected Handler mHandler;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.tvw_loading)
	protected TextView mTvwLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.etx_mail)
	protected EditText mEtxMail;

	@ViewById(R.id.btn_submit)
	protected FrameLayout mBtnSubmit;

	protected boolean actionPerformed = false;
	protected boolean isSmallScreen = false;

	public static FrgRequestFolio instance() {
		return FrgRequestFolio_.builder().build();
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Solicitar folio");

		mHandler = new Handler();
		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(R.string.tvw_request_folio);
			Picasso.with(mContext).load(R.drawable.header_03_results).into(mIvwHeader);
		}

		mLltLoading.setVisibility(View.GONE);
		mTvwLoading.setText(R.string.tvw_searching);

		mEtxMail.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				etxFolioEditorAction(v, actionId, event);
				return false;
			}
		});

		if (mActivity != null) {
			InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
			if (imm != null) {
				imm.hideSoftInputFromWindow(mEtxMail.getWindowToken(), 0);
			}
		}
		// getDataFromDB();
	}

	// @Background
	// protected void getDataFromDB() {
	//
	// }
	//
	// @UiThread
	// protected void setDataFromDB() {
	//
	// }

	protected boolean etxFolioEditorAction(TextView v, int actionId, KeyEvent event) {
		if (!actionPerformed) {
			AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".etxFolioEditorAction", "Solicitar folio");

			actionPerformed = true;
			// if (event != null) {

			InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mEtxMail.getWindowToken(), 0);

			callFolioRequestService();
			// }
			actionPerformed = false;
		}
		return true;
	}

	@Click(R.id.btn_submit)
	protected void mButton() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".etxFolioEditorAction", "Solicitar folio");

		callFolioRequestService();
	}

	protected void callFolioRequestService() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mButtonSearch", "Procesando solicitud de folio");

		InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

		mBtnSubmit.setEnabled(false);
		if (NetworkCheckerUtil.checkNetwork(mActivity)) {

			String email = mEtxMail.getText().toString().trim();
			if ((email.length() > 1) && (email.contains("@"))) {

				mTvwLoading.setText(R.string.dlg_requestion_folio);
				mLltLoading.setVisibility(View.VISIBLE);

				Request.callFolioRequestService(mActivity, email);
			} else {
				mBtnSubmit.setEnabled(true);
				ToastUtil.longToast(mActivity, R.string.toast_fill_the_form);
			}
		} else {
			mBtnSubmit.setEnabled(true);
			ToastUtil.longToast(mActivity, R.string.toast_no_network_connection);
		}
	}

	protected void onEvent(final FolioRequestEvent event) {
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				Request r = event.getRequest();
				if (r != null) {
					if (r.getRequestStatus().equals(C.Status.OK)) {
						ToastUtil.longToast(mActivity, R.string.toast_email_send);
					} else {
						ToastUtil.longToast(mActivity, R.string.toast_no_email);
					}
				} else {
					ToastUtil.longToast(mActivity, R.string.tvw_service_not_available);
				}
				mLltLoading.setVisibility(View.GONE);
			}
		});

	}

	protected void onEvent(final ShowMessageEvent event) {
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				ToastUtil.longToast(mActivity, event.getMessage());
				mLltLoading.setVisibility(View.GONE);
			}
		});
	}
}
