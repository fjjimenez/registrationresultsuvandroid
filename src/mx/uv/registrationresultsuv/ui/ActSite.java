package mx.uv.registrationresultsuv.ui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Date;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.SitePagerAdapter;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.CenterAtLocationEvent;
import mx.uv.registrationresultsuv.interfaces.SlidingPanelActivityInterface;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.DatabaseUtil;
import mx.uv.registrationresultsuv.util.IntentUtil;
import mx.uv.registrationresultsuv.util.NetworkCheckerUtil;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.mobandme.ada.exceptions.AdaFrameworkException;
import com.squareup.picasso.Picasso;

@EActivity(R.layout.act_site)
@OptionsMenu(R.menu.act_site)
public class ActSite extends SherlockFragmentActivity implements SlidingPanelActivityInterface, LocationListener, GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

	protected String mCaller = ActSite.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;

	@Pref
	protected Preferences_ mPreferences;

	private LocationRequest locationRequest;
	private LocationClient locationClient;
	protected Handler mHandler;

	@ViewById(R.id.vwp_sections)
	protected ViewPager mVwpSection;

	protected SitePagerAdapter mAdapter;

	protected ActionBar mActionBar;

	protected MenuItem mMenuItemSiteLocation;
	protected MenuItem mMenuItemMyLocation;

	protected Site mSite;
	protected Location mUserLocation;
	protected Date mLastRequest;

	protected boolean isSmallScreen = false;

	protected boolean isUserAlreadyLocated = false;
	protected boolean isGooglePlayServicesAvailable = false;

	@Extra
	protected Long mID;

	protected int orientation = -1;
	protected int width = -1;
	protected int height = -1;

	@Override
	public void onResume() {
		super.onResume();
		// BusProvider.getInstance().register(this);
		int response = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (response == ConnectionResult.SUCCESS) {
			locationClient = new LocationClient(mContext, this, this);
			locationClient.connect();
			isGooglePlayServicesAvailable = true;
			modifyActionBarButtonsVisibility();
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		stopLocation();
		BusProvider.getInstance().unregister(this);

	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mContext = getApplicationContext();
		isSmallScreen = UiUtil.isCloseToOneAspectRatio(mActivity);

		// if(!isSmallScreen){
		this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		// }
		mActionBar = UiUtil.setupActionBarWithHome(this, isSmallScreen);

		mActionBar.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.ab_black_alpha_background));

		try {
			final ViewConfiguration config = ViewConfiguration.get(this);
			final Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (final Exception ex) {
			// Ignore
		}

		orientation = UiUtil.getScreenOrientation(mActivity);
		Point size = UiUtil.getScreenSize(mActivity);

		width = size.x;
		height = size.y;
	}

	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {

		this.mMenuItemSiteLocation = menu.findItem(R.id.ic_action_location);
		this.mMenuItemMyLocation = menu.findItem(R.id.ic_action_locate_me);

		this.modifyActionBarButtonsVisibility();

		return true;
	}

	@OptionsItem
	protected void homeSelected() {
		finish();
	}

	@OptionsItem(R.id.ic_action_location)
	protected void icActionSetDefaultLocation() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".setDefaultLocation", "Mostrar pin de sede " + mSite.getName() + " " + mSite.getCampusName(mActivity));
		BusProvider.getInstance().post(new CenterAtLocationEvent(C.LOCATION_TYPE.SITE, new LatLng(mSite.getLatitude(), mSite.getLongitude())));
	}

	@OptionsItem(R.id.ic_action_locate_me)
	protected void icActionGetUserLocation() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".getMyLocation", "Mostrar ubicacion de usuario");

		Calendar cal = Calendar.getInstance();
		Date time = cal.getTime();

		if (mLastRequest != null) {
			if ((time.getTime() - mLastRequest.getTime()) > (5 * 60 * 1000)) {
				mUserLocation = null;
			}
		}

		if (mUserLocation == null) {
			startLocation();
		} else if ((locationClient.isConnected()) && (mUserLocation != null)) {
			BusProvider.getInstance().post(new CenterAtLocationEvent(C.LOCATION_TYPE.USER, new LatLng(mUserLocation.getLatitude(), mUserLocation.getLongitude())));
		}
	}

	@OptionsItem(R.id.ic_action_google_maps)
	protected void icActionOpenWithGoogleMaps() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".openWithGoogleMaps", "Obtener indicaciones para llegar a sede " + mSite.getName() + " " + mSite.getCampusName(mActivity));

		try {
			IntentUtil.openGoogleMaps(mActivity, IntentUtil.buildGoogleMapsAddress(mSite));
		} catch (UnsupportedEncodingException e) {
			Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}

	@OptionsItem(R.id.ic_action_share)
	protected void icActionShare() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".share", "Compartir la direccion de la sede " + mSite.getName() + " " + mSite.getCampusName(mActivity));

		prepareToShare();
	}

	@AfterViews
	protected void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Sede");

		if (mID == -1) {
			finish();
		}
		getDataFromDB();
	}

	@Background
	protected void getDataFromDB() {
		Persistence persistence;
		try {
			persistence = DatabaseUtil.getInstance(getApplicationContext(), mCaller + ".getDataFromDB()");
			mSite = Persistence.getSite(persistence, mID);

		} catch (AdaFrameworkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			persistence = null;
			DatabaseUtil.releaseDB(mCaller + ".getDataFromDB()");
		}

		setDataFromDB();
	}

	@UiThread
	protected void setDataFromDB() {
		if (mSite != null) {
			if (NetworkCheckerUtil.checkNetwork(mActivity)) {
				try {
					Picasso.with(mContext).load(IntentUtil.buildGoogleMapsImageAddress(mSite, 512, 512, 1)).fetch();
					// Picasso.with(mContext).load(IntentUtil.buildGoogleMapsImageAddress(mSite,
					// width, height, orientation)).fetch();
				} catch (UnsupportedEncodingException e) {
					Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
					// e.printStackTrace();
				} // .into(mIvwStreetView);
			} else {
				// B4:A0:61:06:EB:E4:19:B4:C1:D0:BA:89:5B:41:31:42:B5:44:1E:73

				isGooglePlayServicesAvailable = false;
				this.modifyActionBarButtonsVisibility();
			}
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			ft.replace(R.id.flt_site, FrgSite.instance(mSite));
			ft.commit();
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLocationChanged(Location location) {
		if (location != null) {
			mUserLocation = location;
			if (!isUserAlreadyLocated) {
				isUserAlreadyLocated = true;
				icActionGetUserLocation();
			}
		}
	}

	protected void startLocation() {
		locationRequest = LocationRequest.create();
		locationRequest.setInterval(120 * 1000);
		locationRequest.setFastestInterval(60 * 1000); // the fastest rate in
														// milliseconds at which
														// your app can handle
														// location updates
		locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
		locationClient.requestLocationUpdates(locationRequest, this);
	}

	protected void stopLocation() {
		if ((locationClient != null) && locationClient.isConnected()) {
			locationClient.removeLocationUpdates(this);
			locationClient.disconnect();
		}
	}

	@Background
	protected void prepareToShare() {
		try {
			share(Picasso.with(mContext).load(IntentUtil.buildGoogleMapsImageAddress(mSite, 300, 300, 1)).get());
		} catch (IOException e) {
			// e.printStackTrace();

			try {
				IntentUtil.shareAddress(mActivity, mSite);
			} catch (NotFoundException e1) {
				Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
				// e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
				// e1.printStackTrace();
			}
		}
	}

	@UiThread
	protected void share(Bitmap bitmap) {
		try {
			IntentUtil.shareAddress(mActivity, mSite, bitmap);
		} catch (NotFoundException e) {
			Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
			// e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			Toast.makeText(mActivity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
			// e.printStackTrace();
		}
	}

	public void modifyActionBarButtonsVisibility() {
		if (mMenuItemSiteLocation != null) {
			this.mMenuItemSiteLocation.setVisible(isGooglePlayServicesAvailable);
		}

		if (mMenuItemMyLocation != null) {
			this.mMenuItemMyLocation.setVisible(isGooglePlayServicesAvailable);
		}
	}

	@Override
	public void setActionBarVisibility(boolean visible) {
		if (this.mActionBar != null) {
			if ((visible) && (!mActionBar.isShowing())) {
				this.mActionBar.show();
			} else if ((!visible) && (mActionBar.isShowing())) {
				this.mActionBar.hide();
			}
		}
	}

	@Override
	public boolean onKeyUp(final int keyCode, final KeyEvent event) {
		if (android.os.Build.VERSION.SDK_INT < 11) {
			if ((event.getAction() == KeyEvent.ACTION_UP) && (keyCode == KeyEvent.KEYCODE_MENU)) {
				this.openOptionsMenu();
				return true;
			}
		}
		return super.onKeyUp(keyCode, event);
	}

	@Override
	public boolean isSmallScreen() {
		return isSmallScreen;
	}
}
