/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.OpenStepEvent;
import mx.uv.registrationresultsuv.events.ShareResultEvent;
import mx.uv.registrationresultsuv.model.Result;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * 
 * @author frank
 */
@EViewGroup(R.layout.cell_result)
public class CellResult extends RelativeLayout {

	protected Context mContext;

	@ViewById(R.id.tvw_career)
	protected TextView mTvwCareer;

	@ViewById(R.id.tvw_accepted)
	protected TextView mTvwAccepted;

	@ViewById(R.id.tvw_place)
	protected TextView mTvwNumber;

	@ViewById(R.id.tvw_area)
	protected TextView mTvwArea;

	@ViewById(R.id.tvw_region)
	protected TextView mTvwRegion;

	@ViewById(R.id.tvw_type)
	protected TextView mTvwType;

	@ViewById(R.id.ivw_icon)
	protected ImageView mIvwIcon;

	@ViewById(R.id.btn_whats_next)
	protected Button mBtnWhatsNext;

	@ViewById(R.id.btn_share)
	protected Button mBtnShare;

	protected Result mResult;

	public CellResult(Context context) {
		super(context);
		mContext = context;
	}

	public void bind(final Result result) {
		mResult = result;

		mTvwCareer.setText(mResult.getCareer());
		mTvwAccepted.setText(mResult.getAccepted());
		mTvwNumber.setText(" " + String.valueOf(mResult.getPlace()));

		mTvwArea.setText(mResult.getArea());
		mTvwRegion.setText(mResult.getRegion());
		mTvwType.setText(mResult.getType());

		if (mResult.isAccepted()) {
			mIvwIcon.setVisibility(View.VISIBLE);
			Picasso.with(getContext()).load(R.drawable.ic_results).into(mIvwIcon);
		} else {
			mIvwIcon.setVisibility(View.GONE);
		}
	}

	@Click(R.id.btn_whats_next)
	protected void mBtnWhatsNext() {
		int index = 6;
		if (mResult.isAccepted()) {
			index = 8;
		}
		BusProvider.getInstance().post(new OpenStepEvent(index));
	}

	@Click(R.id.btn_share)
	protected void mBtnShare() {
		BusProvider.getInstance().post(new ShareResultEvent(mResult));
	}

}
