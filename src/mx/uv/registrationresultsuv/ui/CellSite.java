/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.model.Site;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * 
 * @author frank
 */
@EViewGroup(R.layout.cell_site)
public class CellSite extends RelativeLayout {

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_icon)
	protected ImageView mIvwIcon;

	public CellSite(Context context) {
		super(context);
	}

	public void bind(final Site item) {
		mTvwTitle.setText(item.getName());

		Picasso.with(getContext()).load(R.drawable.ic_location).into(mIvwIcon);
	}

}
