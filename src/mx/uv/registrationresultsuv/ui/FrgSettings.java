package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.IntentUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.CheckedChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_settings)
public class FrgSettings extends SherlockFragment {

	protected String mCaller = FrgSettings.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected ActivityInterface mCallback;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.tvw_privacy_declaration_url)
	protected TextView mTvwPrivacyDeclarationUrl;

	@ViewById(R.id.tvw_privacy_declaration_contact)
	protected TextView mTvwPrivacyDeclarationContac;

	@ViewById(R.id.chk_analitycs)
	protected CheckBox mChkAnalitycs;

	protected boolean isSmallScreen = false;

	public static FrgSettings instance() {
		return FrgSettings_.builder().build();
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {

		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Configuracion");

		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(R.string.tvw_settings);
			Picasso.with(mContext).load(R.drawable.header_04_settings).into(mIvwHeader);
		}

		mChkAnalitycs.setChecked(mPreferences.IsAnalitycsAccepted().get());

		// mLltLoading.setVisibility(View.GONE);
	}

	@CheckedChange(R.id.chk_analitycs)
	protected void mChkAnalitycs(CompoundButton buttonView, boolean isChecked) {
		mPreferences.IsAnalitycsAccepted().put(isChecked);
	}

	@Click(R.id.tvw_privacy_declaration_url)
	protected void mTvwPrivacyDeclarationUrl() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mTvwPrivacyDeclarationUrl", "Abrir declarativa de privacidad");
		IntentUtil.openWebUrl(mActivity, C.STATIC_PRIVACY_DECLARATION_URL);
	}

	@Click(R.id.tvw_privacy_declaration_contact)
	protected void mTvwPrivacyDeclarationContac() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mTvwPrivacyDeclarationContac", "Enviar por correo comentarios de configuracion al desarrollador");
		IntentUtil.sendMail(mActivity, mActivity.getString(R.string.app_name), C.STATIC_DEV_MAIL);
	}
}
