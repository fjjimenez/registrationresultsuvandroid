package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.MenuItemAdapter;
import mx.uv.registrationresultsuv.adapters.SectionPagerAdapter;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.MenuDrawerItem;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.DateUtil;
import mx.uv.registrationresultsuv.util.DialogUtil;
import mx.uv.registrationresultsuv.util.IntentUtil;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.res.StringArrayRes;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;
import com.sherlock.navigationdrawer.compat.SherlockActionBarDrawerToggle;
import com.tjeannin.apprate.AppRate;
import com.tjeannin.apprate.PrefsContract;

@EActivity(R.layout.main_activity)
public class MainActivity extends SherlockFragmentActivity implements ActivityInterface {

	protected final String mCaller = MainActivity.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;

	@Pref
	protected Preferences_ mPreferences;

	protected Handler mHandler;

	@ViewById(R.id.dlt_root)
	protected DrawerLayout mDltRoot;

	@ViewById(R.id.llt_drawer)
	protected LinearLayout mLltDrawer;

	@ViewById(R.id.vw_drawer_header)
	protected View mVwDrawerHeader;

	@ViewById(R.id.lvw_drawer)
	protected ListView mLvwDrawer;

	@ViewById(R.id.vwp_sections)
	protected ViewPager mVwpSection;

	protected ActionBar mActionBar;

	@StringArrayRes(R.array.menu_items_array)
	protected String[] mMenuItems;

	protected SherlockActionBarDrawerToggle mDrawerToggle;

	protected SectionPagerAdapter mSectionAdapter;

	protected MenuItemAdapter mAdapter;

	protected boolean isSmallScreen = false;

	protected boolean setup = false;
	protected boolean actionPerformed = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mContext = getApplicationContext();

		isSmallScreen = UiUtil.isCloseToOneAspectRatio(mActivity);

		if (!isSmallScreen) {
			this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		}
		mActionBar = UiUtil.setupActionBarWithHome(this, isSmallScreen);
		mActionBar.setTitle(R.string.blank);

		if (this.mPreferences.DatabaseVersion().get() < C.MIN_DATABASE_VERSION) {
			try {
				Persistence persistence = new Persistence(this.getApplicationContext());
				persistence.deleteDatabase();
				persistence = null;
				this.mPreferences.DatabaseVersion().put(C.MIN_DATABASE_VERSION);
			} catch (final Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@OptionsItem
	protected void homeSelected() {
		// menu.toggle();
		if (mDltRoot.isDrawerOpen(mLltDrawer)) {
			mDltRoot.closeDrawers();
		} else {
			mDltRoot.openDrawer(mLltDrawer);
		}
	}

	@AfterViews
	protected void setup() {
		if (C.BLACKBERRY_10_BUILD) {
			if (!mPreferences.IsAnalitycsDialogAlreadyShowed().get()) {
				mPreferences.IsAnalitycsAccepted().put(false);
				mPreferences.IsAnalitycsDialogAlreadyShowed().put(true);
				DialogUtil.showAcceptAnalitycsDialog(mActivity, mPreferences, new AnalitycsDialogClickListener());
			}
		}

		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Principal");

		mSectionAdapter = new SectionPagerAdapter(getSupportFragmentManager());
		mVwpSection.setAdapter(mSectionAdapter);

		if (isSmallScreen) {
			mVwpSection.setOnPageChangeListener(new SectionOnPageChangeListener());
			mVwDrawerHeader.setVisibility(View.GONE);

			if (DateUtil.isTimeToShowResults()) {
				this.changeTitle(SectionPagerAdapter.getItemTitle(2));
			} else {
				this.changeTitle(SectionPagerAdapter.getItemTitle(0));
			}
		}

		if (DateUtil.isTimeToShowResults()) {
			mVwpSection.setCurrentItem(2);
		}

		setupActionBarDrawerToggle();
		showRateDialog();
	}

	@ItemClick(R.id.lvw_drawer)
	protected void lvwDrawer(final MenuDrawerItem item) {
		if (mSectionAdapter != null) {
			mVwpSection.setCurrentItem(item.getPosition());
			mDltRoot.closeDrawers();
		}
	}

	protected void setupActionBarDrawerToggle() {
		mAdapter = new MenuItemAdapter(getApplicationContext(), R.layout.row_menu_item, MenuDrawerItem.getMenuItems(mMenuItems));

		mLvwDrawer.setAdapter(mAdapter);

		mDrawerToggle = new SherlockActionBarDrawerToggle(this, mDltRoot, R.drawable.ic_navigation_drawer, R.string.drawer_open, R.string.drawer_open) {
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				if (!isSmallScreen) {
					mActionBar.setTitle(R.string.drawer_open);
				} else {
					changeTitle(SectionPagerAdapter.getItemTitle(mVwpSection.getCurrentItem()));
				}
			}

			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				mActionBar.setTitle(R.string.drawer_close);
			}
		};

		mDltRoot.setDrawerListener(mDrawerToggle);
		mDrawerToggle.syncState();
	}

	protected void changeTitle(int resourceId) {
		if (mActionBar != null) {
			mActionBar.setTitle(resourceId);
		}
	}

	@Override
	public boolean isSmallScreen() {
		return isSmallScreen;
	}

	public void showRateDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this).setMessage(R.string.tvw_rate_us).setPositiveButton(R.string.btn_yes, null).setNegativeButton(R.string.btn_no, null).setNeutralButton(R.string.btn_cancel, null);
		new AppRate(mActivity).setOnClickListener(new RateUsDialogClickListener()).setMinLaunchesUntilPrompt(C.RATE_APP_MIN_LAUNCHES_UNTIL_PROMPT).setCustomDialog(builder).init();
	}

	protected class RateUsDialogClickListener implements android.content.DialogInterface.OnClickListener {
		private SharedPreferences preferences;

		@Override
		public void onClick(DialogInterface dialog, int which) {
			preferences = mActivity.getSharedPreferences(PrefsContract.SHARED_PREFS_NAME, 0);
			Editor editor = preferences.edit();

			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				if (C.BLACKBERRY_10_BUILD) {
					IntentUtil.openWebUrl(mActivity, C.BLACKBERRY_WORLD_APP_URL);
				} else {
					IntentUtil.openGooglePlayForRating(mActivity);
				}

				editor.putBoolean(PrefsContract.PREF_DONT_SHOW_AGAIN, true);
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				editor.putBoolean(PrefsContract.PREF_DONT_SHOW_AGAIN, true);
				break;

			case DialogInterface.BUTTON_NEUTRAL:
				editor.putLong(PrefsContract.PREF_DATE_FIRST_LAUNCH, System.currentTimeMillis());
				editor.putLong(PrefsContract.PREF_LAUNCH_COUNT, 0);
				break;

			default:
				break;
			}

			editor.commit();
			dialog.dismiss();

		}
	}

	protected class AnalitycsDialogClickListener implements android.content.DialogInterface.OnClickListener {
		@Override
		public void onClick(DialogInterface dialog, int which) {

			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				mPreferences.IsAnalitycsAccepted().put(true);
				break;
			default:
				break;
			}

			dialog.dismiss();

		}
	}

	protected class SectionOnPageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int state) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int page) {
			changeTitle(SectionPagerAdapter.getItemTitle(page));
		}
	}

}
