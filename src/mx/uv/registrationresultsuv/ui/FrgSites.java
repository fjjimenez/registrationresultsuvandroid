package mx.uv.registrationresultsuv.ui;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.StickyHeaderSiteAdapter;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.SitesDownloadedEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.DatabaseUtil;
import mx.uv.registrationresultsuv.util.NetworkCheckerUtil;
import mx.uv.registrationresultsuv.util.ToastUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobandme.ada.exceptions.AdaFrameworkException;
import com.squareup.picasso.Picasso;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersGridView;

@EFragment(R.layout.frg_generic_grid_with_sticky_header)
public class FrgSites extends SherlockFragment {

	protected String mCaller = FrgSites.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected Handler mHandler;
	protected ActivityInterface mCallback;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.gvw_items)
	protected StickyGridHeadersGridView mGvwSites;

	@ViewById(R.id.llt_no_internet)
	protected LinearLayout mLltNoInternet;

	@ViewById(R.id.ivw_empty)
	protected ImageView mIvwEmpty;

	protected StickyHeaderSiteAdapter mAdapter;

	protected ArrayList<Site> mSites;

	protected boolean isVisibleToUser = false;
	protected boolean isNetworkAvailable = false;
	protected boolean isSmallScreen = false;

	public static FrgSites instance() {
		return FrgSites_.builder().build();
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		this.isVisibleToUser = isVisibleToUser;
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);

	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isNetworkAvailable = NetworkCheckerUtil.checkNetwork(mActivity);
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Sedes");

		mHandler = new Handler();

		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(R.string.tvw_hq);
			Picasso.with(mContext).load(R.drawable.header_02_sites).into(mIvwHeader);
		}

		getDataFromDB();
	}

	@Background
	public void getDataFromDB() {

		try {
			Persistence persistence = DatabaseUtil.getInstance(mContext, mCaller + ".getDataFromDB()");
			mSites = Persistence.getSites(mActivity, mPreferences, persistence, isNetworkAvailable);
			if ((mSites != null) && (!mSites.isEmpty())) {
				mAdapter = new StickyHeaderSiteAdapter(mContext, R.layout.cell_site_header, R.layout.cell_step, mSites);
			} else {
				if (!isNetworkAvailable) {
					showEmptyList();
				}
			}
		} catch (AdaFrameworkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DatabaseUtil.releaseDB(mCaller + ".getDataFromDB()");
		}

		setDataFromDB();
	}

	@UiThread
	public void setDataFromDB() {
		if ((mSites != null) && (!mSites.isEmpty())) {
			mGvwSites.setAdapter(mAdapter);
			mLltLoading.setVisibility(View.GONE);
		}
	}

	@ItemClick(R.id.gvw_items)
	protected void gvwItems(final int position) {
		if ((mSites != null) && (!mSites.isEmpty())) {
			ActSite_.intent(mActivity).mID(mSites.get(position).getID()).start();
		}
	}

	public void onEvent(final SitesDownloadedEvent event) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if ((event.getSites() != null) && (!event.getSites().isEmpty())) {
					mSites = event.getSites();
					mAdapter = new StickyHeaderSiteAdapter(mContext, R.layout.cell_site_header, R.layout.cell_site, mSites);
				} else if ((mSites == null) || (mSites.isEmpty())) {
					showEmptyList();
				}
				setDataFromDB();
			}
		});
	}

	@UiThread
	protected void showEmptyList() {
		try {
			mLltLoading.setVisibility(View.GONE);
			mGvwSites.setVisibility(View.GONE);
			mLltNoInternet.setVisibility(View.VISIBLE);
			Picasso.with(mContext).load(R.drawable.ic_empty).into(mIvwEmpty);
			if (isVisibleToUser) {
				ToastUtil.longToast(mActivity, R.string.ctn_service_unavailable);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}
}
