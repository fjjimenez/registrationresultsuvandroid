/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.model.Aspirant;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * 
 * @author frank
 */
@EViewGroup(R.layout.cell_aspirant)
public class CellAspirant extends RelativeLayout {

	@ViewById(R.id.tvw_name)
	protected TextView mTvwName;

	@ViewById(R.id.ivw_icon)
	protected ImageView mIvwIcon;

	public CellAspirant(Context context) {
		super(context);
	}

	public void bind(final Aspirant aspirant) {
		mTvwName.setText(aspirant.getName());

		Picasso.with(getContext()).load(R.drawable.ic_aspirant).into(mIvwIcon);
	}

}
