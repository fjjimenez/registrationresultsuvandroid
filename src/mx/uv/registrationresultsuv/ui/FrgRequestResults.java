package mx.uv.registrationresultsuv.ui;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.AspirantAdapter;
import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.SearchRequestEvent;
import mx.uv.registrationresultsuv.events.ShowMessageEvent;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Aspirant;
import mx.uv.registrationresultsuv.model.Request;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.DateUtil;
import mx.uv.registrationresultsuv.util.NetworkCheckerUtil;
import mx.uv.registrationresultsuv.util.ToastUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.SherlockFragment;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_request_results)
public class FrgRequestResults extends SherlockFragment {

	protected String mCaller = FrgRequestResults.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected ActivityInterface mCallback;
	protected Handler mHandler;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.tvw_loading)
	protected TextView mTvwLoading;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.vw_upper_padding)
	protected View mVwUpperPadding;

	@ViewById(R.id.vw_lower_padding)
	protected View mVwLowerPadding;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	@ViewById(R.id.etx_search)
	protected EditText mEtxSearch;

	@ViewById(R.id.btn_search)
	protected FrameLayout mBtnSearch;

	@ViewById(R.id.tvw_message)
	protected TextView mTvwMessage;

	@ViewById(R.id.gvw_aspirants)
	protected GridView mGvwAspirants;

	@ViewById(R.id.btn_request_folio)
	protected Button mBtnRequestFolio;

	protected AspirantAdapter mAspirantAdapter;

	protected ArrayList<Aspirant> mAspirants;

	protected boolean actionPerformed = false;
	protected boolean isSmallScreen = false;

	public static FrgRequestResults instance() {
		return FrgRequestResults_.builder().build();
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onResume() {
		super.onResume();
		BusProvider.getInstance().register(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		BusProvider.getInstance().unregister(this);
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Resultados");

		mHandler = new Handler();
		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(R.string.tvw_results);
			Picasso.with(mContext).load(R.drawable.header_03_results).into(mIvwHeader);
		}

		mLltLoading.setVisibility(View.GONE);
		mTvwLoading.setText(R.string.tvw_searching);

		mEtxSearch.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				etxMailEditorAction(v, actionId, event);
				return false;
			}
		});

		if (DateUtil.isOver9OfJuly()) {
			mTvwMessage.setVisibility(View.GONE);
		}

		// getDataFromDB();
	}

	// @Background
	// protected void getDataFromDB() {
	//
	// }
	//
	// @UiThread
	// protected void setDataFromDB() {
	//
	// }

	protected void updateAspirantsGridView() {
		if (mGvwAspirants.getVisibility() != View.VISIBLE) {
			mGvwAspirants.setVisibility(View.VISIBLE);
			mVwUpperPadding.setVisibility(View.GONE);
			mVwLowerPadding.setVisibility(View.GONE);

			if (!DateUtil.isOver9OfJuly()) {
				mTvwMessage.setVisibility(View.GONE);
			}
		}

		if ((mAspirantAdapter == null) && ((mAspirants != null) && (!mAspirants.isEmpty()))) {
			mAspirantAdapter = new AspirantAdapter(mActivity, R.layout.cell_aspirant, mAspirants);
			mGvwAspirants.setAdapter(mAspirantAdapter);
		} else if ((mAspirantAdapter != null) && ((mAspirants != null) && (!mAspirants.isEmpty()))) {
			mAspirantAdapter.clear();
			mAspirantAdapter.addAll(mAspirants);
			mAspirantAdapter.notifyDataSetChanged();
		} else {
			mVwUpperPadding.setVisibility(View.VISIBLE);
			mVwLowerPadding.setVisibility(View.VISIBLE);

			if (!DateUtil.isOver9OfJuly()) {
				mTvwMessage.setVisibility(View.VISIBLE);
			}
			mGvwAspirants.setVisibility(View.GONE);
		}
	}

	protected boolean etxMailEditorAction(TextView v, int actionId, KeyEvent event) {
		if (!actionPerformed) {
			AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".etxMailEditorAction", "Buscar resultado de aspirante");

			actionPerformed = true;

			InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mEtxSearch.getWindowToken(), 0);

			callSearchService();
			actionPerformed = false;
		}
		return true;
	}

	@Click(R.id.btn_search)
	protected void mButtonSearch() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mButtonSearch", "Buscar resultado de aspirante");
		callSearchService();
	}

	@Click(R.id.btn_request_folio)
	protected void mButtonForgotFolio() {
		ActRequestFolio_.intent(mActivity).start();
	}

	protected void callSearchService() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mButtonSearch", "Procesando busqueda resultado de aspirante");

		InputMethodManager inputManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

		mBtnSearch.setEnabled(false);
		if (NetworkCheckerUtil.checkNetwork(mActivity)) {
			String query = mEtxSearch.getText().toString().trim();
			if (query.length() > 1) {
				mLltLoading.setVisibility(View.VISIBLE);
				Request.callSearchRequestService(mActivity, query);
			} else {
				mBtnSearch.setEnabled(true);
				ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_fill_the_form));
			}
		} else {
			mBtnSearch.setEnabled(true);
			ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_no_network_connection));
		}
	}

	@ItemClick(R.id.gvw_aspirants)
	protected void mGvwAspirants(Aspirant aspirant) {
		ActAspirantResults_.intent(mActivity).mAspirant(aspirant).start();
	}

	protected void onEvent(final SearchRequestEvent event) {
		mHandler.post(new Runnable() {

			@Override
			public void run() {
				mLltLoading.setVisibility(View.GONE);
				mBtnSearch.setEnabled(true);

				Request request = event.getRequest();
				switch (request.getRequestStatus()) {
				case NA:
					if (DateUtil.isOver9OfJuly()) {
						ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_wait));
					} else {
						ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_warning));
					}
					break;
				case OK:
					mAspirants = request.getPersons();
					if ((mAspirants != null) && (!mAspirants.isEmpty())) {
						/*
						 * Aspirant aspirant = mAspirants.get(0); Result result
						 * = aspirant.getResults().get(0);
						 * result.setPlace(1000);
						 * result.setAccepted("SIN DERECHO");
						 * result.setMode("FF");
						 * result.setCareer("INFORMATICA");
						 * result.setArea("AREA ECONOMICO ADMINISTRATIVO");
						 * aspirant.getResults().add(result);
						 * mAspirants.add(aspirant);
						 */
						updateAspirantsGridView();
					} else {
						ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_no_results));
					}
					break;
				case TOO_MANY_RESULTS:
					mAspirants = request.getPersons();
					if ((mAspirants != null) && (!mAspirants.isEmpty())) {
						updateAspirantsGridView();
						ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_too_many_results));
					} else {
						ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_no_results));
					}
					break;
				default:
					ToastUtil.longToast(mActivity, mActivity.getString(R.string.toast_service_unavailable));
					break;
				}
			}
		});
	}

	protected void onEvent(final ShowMessageEvent event) {
		mHandler.post(new Runnable() {
			@Override
			public void run() {
				ToastUtil.longToast(mActivity, event.getMessage());
			}
		});
	}
}
