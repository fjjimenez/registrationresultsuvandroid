package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.IntentUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.squareup.picasso.Picasso;

@EFragment(R.layout.frg_about)
public class FrgAbout extends SherlockFragment {

	protected String mCaller = FrgAbout.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected ActivityInterface mCallback;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.rlt_header)
	protected RelativeLayout mRltHeader;

	@ViewById(R.id.llt_loading)
	protected LinearLayout mLltLoading;

	@ViewById(R.id.tvw_title)
	protected TextView mTvwTitle;

	@ViewById(R.id.ivw_header)
	protected ImageView mIvwHeader;

	protected boolean isSmallScreen = false;

	public static FrgAbout instance() {
		return FrgAbout_.builder().build();
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		try {
			mCallback = (ActivityInterface) activity;

			mActivity = activity;
			mContext = activity.getApplicationContext();
			isSmallScreen = mCallback.isSmallScreen();
		} catch (final ClassCastException e) {

		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@AfterViews
	public void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Acerca de");

		if (isSmallScreen) {
			mRltHeader.setVisibility(View.GONE);
		} else {
			mTvwTitle.setText(R.string.tvw_about_the_app);
			Picasso.with(mContext).load(R.drawable.header_05_about).into(mIvwHeader);
		}
	}

	@Click(R.id.tvw_email)
	protected void mTvwSendEmail() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mTvwSendEmail", "Enviar por correo comentarios al desarrollador");
		IntentUtil.sendMail(mActivity, mActivity.getString(R.string.app_name), C.STATIC_DEV_MAIL);
	}

	@Click(R.id.tvw_blog)
	protected void mTvwOpenDevSite() {
		AnalitycsUtil.trackEvent(mActivity, mPreferences, "UI", mCaller + ".mTvwOpenDevSite", "Abrir url del desarrollado");
		IntentUtil.openWebUrl(mActivity, C.STATIC_DEV_SITE_URL);
	}
}
