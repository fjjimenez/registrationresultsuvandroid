/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.ui;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;

/**
 * 
 * @author frank
 */

@EActivity(R.layout.act_request_folio)
public class ActRequestFolio extends SherlockFragmentActivity implements ActivityInterface {

	protected String mCaller = ActAspirantResults.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;
	protected Handler mHandler;

	@Pref
	protected Preferences_ mPreferences;

	protected ActionBar mActionBar;

	protected boolean isSmallScreen = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mContext = getApplicationContext();
		isSmallScreen = UiUtil.isCloseToOneAspectRatio(mActivity);

		if (!isSmallScreen) {
			this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		}
		mActionBar = UiUtil.setupActionBarWithHome(this, isSmallScreen);

		if (isSmallScreen) {
			changeTitle(R.string.tvw_request_folio);
		}
	}

	@OptionsItem
	protected void homeSelected() {
		finish();
	}

	@AfterViews
	protected void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Solicitar folio");

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.flt_form, FrgRequestFolio.instance());
		ft.commit();
	}

	protected void changeTitle(int resourceId) {
		if (mActionBar != null) {
			mActionBar.setTitle(resourceId);
		}
	}

	@Override
	public boolean isSmallScreen() {
		return isSmallScreen;
	}
}
