package mx.uv.registrationresultsuv.ui;

import java.util.Date;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.adapters.SitePagerAdapter;
import mx.uv.registrationresultsuv.interfaces.ActivityInterface;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.UiUtil;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Window;

@EActivity(R.layout.act_generic_fragment)
public class ActSites extends SherlockFragmentActivity implements ActivityInterface {

	protected String mCaller = ActSites.class.getSimpleName();

	protected Activity mActivity;
	protected Context mContext;

	@Pref
	protected Preferences_ mPreferences;

	@ViewById(R.id.vwp_sections)
	protected ViewPager mVwpSection;

	protected SitePagerAdapter mAdapter;

	protected ActionBar mActionBar;

	protected Site mSite;

	protected Location mUserLocation;

	protected Date mLastRequest;

	protected boolean isSmallScreen = false;

	protected boolean isUserAlreadyLocated = false;
	protected boolean isGooglePlayServicesAvailable = false;

	@Extra
	protected C.FRG_TYPE mType;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mActivity = this;
		mContext = getApplicationContext();
		isSmallScreen = UiUtil.isCloseToOneAspectRatio(mActivity);

		if (!isSmallScreen) {
			this.requestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		}
		mActionBar = UiUtil.setupActionBarWithHome(this, isSmallScreen);

	}

	@OptionsItem
	protected void homeSelected() {
		finish();
	}

	@AfterViews
	protected void setup() {
		AnalitycsUtil.trackView(mActivity, mPreferences, mCaller + ".Sedes");

		if (isSmallScreen) {
			changeTitle(R.string.tvw_hq);
		}

		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		final FrgSites frgSites = FrgSites.instance();
		ft.replace(R.id.flt_content, frgSites);

		ft.commit();
	}

	protected void changeTitle(int resourceId) {
		if (mActionBar != null) {
			mActionBar.setTitle(resourceId);
		}
	}

	@Override
	public boolean isSmallScreen() {
		return isSmallScreen;
	}
}
