/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.services;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.FolioRequestEvent;
import mx.uv.registrationresultsuv.events.ShowMessageEvent;
import mx.uv.registrationresultsuv.model.Request;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.AnalitycsUtil;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.GSONUtil;

import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.apache.http.NameValuePair;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.message.BasicNameValuePair;

import android.app.IntentService;
import android.content.Intent;

/**
 * 
 * @author frank
 */
@EIntentService
public class FolioRequestService extends IntentService {

	protected static String mCaller = FolioRequestService.class.getSimpleName();

	@Pref
	protected Preferences_ mPreferences;

	public FolioRequestService() {
		super("IngresoUV-FolioRequestService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mPreferences = new Preferences_(getApplicationContext());
		
		AnalitycsUtil.trackEvent(this.getApplication(), mPreferences, "Background", mCaller + ".onHandleIntent", "Procesando solicitud de folio");

		String query = intent.getStringExtra(C.QUERY);

		Request r = new Request();

		boolean found = false;
		try {

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			nameValuePairs.add(new BasicNameValuePair("correo", query));

			r = GSONUtil.parseRequest(C.BASE_URL, getApplicationContext(), nameValuePairs);

			BusProvider.getInstance().post(new FolioRequestEvent(r));

			switch (r.getRequestStatus()) {
			case OK:
				AnalitycsUtil.trackEvent(this.getApplication(), mPreferences, "Background", mCaller + ".onHandleIntent", "Folio enviado");
				break;
			case ERROR:
				AnalitycsUtil.trackEvent(this.getApplication(), mPreferences, "Background", mCaller + ".onHandleIntent", "Servicio no disponible");
				break;
			default:
				AnalitycsUtil.trackEvent(this.getApplication(), mPreferences, "Background", mCaller + ".onHandleIntent", "Folio no enviado, correo no registrado");
				break;
			}
			found = true;
		} catch (SocketTimeoutException e) {
			BusProvider.getInstance().post(new ShowMessageEvent(e.getLocalizedMessage(), C.DIALOG_TYPE.ALERT));
			e.printStackTrace();
		} catch (ConnectTimeoutException e) {
			BusProvider.getInstance().post(new ShowMessageEvent(e.getLocalizedMessage(), C.DIALOG_TYPE.ALERT));
			e.printStackTrace();
		} catch (IOException e) {
			BusProvider.getInstance().post(new ShowMessageEvent(e.getLocalizedMessage(), C.DIALOG_TYPE.ALERT));
			e.printStackTrace();
		} catch (Exception e) {
			BusProvider.getInstance().post(new ShowMessageEvent(e.getLocalizedMessage(), C.DIALOG_TYPE.ALERT));
			e.printStackTrace();
		}

		if (!found) {
			AnalitycsUtil.trackEvent(this.getApplication(), mPreferences, "Background", mCaller + ".onHandleIntent", "Servicio no disponible");

		}
	}
}
