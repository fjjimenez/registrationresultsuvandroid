/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.services;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.SitesDownloadedEvent;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.CacheUtil;
import mx.uv.registrationresultsuv.util.DatabaseUtil;
import mx.uv.registrationresultsuv.util.GSONUtil;

import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.IntentService;
import android.content.Intent;

/**
 * 
 * @author frank
 */
@EService
public class SitesRequestService extends IntentService {

	protected String mCaller = SitesRequestService.class.getSimpleName();

	@Pref
	protected Preferences_ mPreferences;

	public SitesRequestService() {
		super("IngresoUV-SiteRequestService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		boolean downloaded = false;
		ArrayList<Site> parsedSites = null;
		try {

			parsedSites = GSONUtil.parseSites(C.STATIC_SITES_URL, getApplicationContext());
			if ((parsedSites != null) && (parsedSites.isEmpty())) {
				downloaded = true;
			} else if ((parsedSites != null) && (!parsedSites.isEmpty())) {
				downloaded = true;

				Persistence persistence = DatabaseUtil.getInstance(getApplicationContext(), mCaller + ".onHandleIntent()");
				persistence.saveSites(parsedSites);
				DatabaseUtil.releaseDB(mCaller + ".onHandleIntent()");

				CacheUtil.cacheSection(mPreferences, C.SECTION.SITES);

			} else if (parsedSites == null) {
				parsedSites = new ArrayList<Site>();
			}
		} catch (Exception e) {
			parsedSites = new ArrayList<Site>();
			e.printStackTrace();
		}
		BusProvider.getInstance().post(new SitesDownloadedEvent(downloaded, parsedSites));
	}
}
