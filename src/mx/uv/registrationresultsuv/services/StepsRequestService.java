/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.services;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.events.BusProvider;
import mx.uv.registrationresultsuv.events.StepsDownloadedEvent;
import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.persistence.Persistence;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.CacheUtil;
import mx.uv.registrationresultsuv.util.DatabaseUtil;
import mx.uv.registrationresultsuv.util.GSONUtil;

import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.IntentService;
import android.content.Intent;

/**
 * 
 * @author frank
 */
@EService
public class StepsRequestService extends IntentService {

	protected String mCaller = StepsRequestService.class.getSimpleName();

	@Pref
	protected Preferences_ mPreferences;

	public StepsRequestService() {
		super("IngresoUV-StepsRequestService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		boolean downloaded = false;
		ArrayList<Step> parseSteps = null;
		try {

			parseSteps = GSONUtil.parseSteps(C.STATIC_STEPS_URL, getApplicationContext());
			if ((parseSteps != null) && (parseSteps.isEmpty())) {
				downloaded = true;
			} else if ((parseSteps != null) && (!parseSteps.isEmpty())) {
				downloaded = true;

				Persistence persistence = DatabaseUtil.getInstance(getApplicationContext(), mCaller + ".onHandleIntent()");
				persistence.saveSteps(parseSteps);
				DatabaseUtil.releaseDB(mCaller + ".onHandleIntent()");

				CacheUtil.cacheSection(mPreferences, C.SECTION.STEPS);

			} else if (parseSteps == null) {
				parseSteps = new ArrayList<Step>();
			}
		} catch (Exception e) {
			parseSteps = new ArrayList<Step>();
		}
		BusProvider.getInstance().post(new StepsDownloadedEvent(downloaded, parseSteps));
	}
}
