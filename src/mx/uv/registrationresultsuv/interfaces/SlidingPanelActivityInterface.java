/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.interfaces;

/**
 * 
 * @author frank
 */
public interface SlidingPanelActivityInterface {
	public boolean isSmallScreen();
	public void setActionBarVisibility(boolean visible);
}
