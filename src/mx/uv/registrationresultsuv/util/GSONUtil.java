package mx.uv.registrationresultsuv.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.GsonWrapperSites;
import mx.uv.registrationresultsuv.model.GsonWrapperSteps;
import mx.uv.registrationresultsuv.model.Request;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.model.Step;

import org.apache.http.NameValuePair;
import org.apache.http.conn.ConnectTimeoutException;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GSONUtil {

	public static ArrayList<Step> parseSteps(String URL, Context context) throws SocketTimeoutException, ConnectTimeoutException, IOException, Exception {
		InputStream mSource = null;

		RequestClient mRequestClient = new RequestClient();
		mSource = mRequestClient.getRequest(URL, context);

		Reader mReader = new InputStreamReader(mSource);

		Gson gson = new Gson();
		Type type = new TypeToken<GsonWrapperSteps>() {
		}.getType();

		GsonWrapperSteps steps = gson.fromJson(mReader, type);
		return steps.getSteps();
	}

	public static ArrayList<Site> parseSites(String URL, Context context) throws SocketTimeoutException, ConnectTimeoutException, IOException, Exception {
		InputStream mSource = null;

		RequestClient mRequestClient = new RequestClient();
		mSource = mRequestClient.getRequest(URL, context);

		Reader mReader = new InputStreamReader(mSource);

		Gson gson = new Gson();
		Type type = new TypeToken<GsonWrapperSites>() {
		}.getType();

		GsonWrapperSites sites = gson.fromJson(mReader, type);
		return sites.getSites();
	}

	public static Request parseRequest(String URL, Context context, ArrayList<NameValuePair> nameValuePairs) throws SocketTimeoutException, ConnectTimeoutException, IOException, Exception {
		InputStream mSource = null;

		RequestClient mRequestClient = new RequestClient();
		mSource = mRequestClient.postRequest(URL, context, nameValuePairs);

		Reader mReader = new InputStreamReader(mSource);

		Gson gson = new Gson();
		Type type = new TypeToken<Request>() {
		}.getType();

		Request requests = gson.fromJson(mReader, type);
		return requests;
	}

}
