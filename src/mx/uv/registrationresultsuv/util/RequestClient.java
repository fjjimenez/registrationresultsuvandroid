/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.content.Context;

/**
 * 
 * @author frank
 */
public class RequestClient {

	public InputStream postRequest(final String url, final Context context, final String json) throws IOException, SocketTimeoutException, ConnectTimeoutException, Exception {
		final HttpPost mHttpPost = new HttpPost(url);

		if ((json != null) && (json.length() > 0)) {
			mHttpPost.setEntity(new StringEntity(json));
			mHttpPost.setHeader("Accept", "application/json");
			mHttpPost.setHeader("Content-type", "application/json");
		}
		return executeRequest(mHttpPost);
	}

	public InputStream postRequest(final String url, final Context context, final ArrayList<NameValuePair> nameValuePairs) throws IOException, SocketTimeoutException, ConnectTimeoutException, Exception {
		final HttpPost mHttpPost = new HttpPost(url);

		if ((nameValuePairs != null) && (!nameValuePairs.isEmpty())) {
			mHttpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		}

		return executeRequest(mHttpPost);
	}

	public InputStream getRequest(final String url, final Context context) throws IOException, SocketTimeoutException, ConnectTimeoutException, Exception {
		final HttpGet mHttpGet = new HttpGet(url);

		mHttpGet.setHeader("Accept", "application/json");

		return executeRequest(mHttpGet);
	}

	public String request(final String url) throws MalformedURLException, IOException {
		final HttpClient mClient = builHttpClient();

		final HttpPost mHttpPost = new HttpPost(url);

		final ResponseHandler<String> mResponseHandler = new BasicResponseHandler();
		final String responseBody = mClient.execute(mHttpPost, mResponseHandler);

		return responseBody;
	}

	private DefaultHttpClient builHttpClient() {
		final HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
		HttpConnectionParams.setSoTimeout(httpParameters, 10000);

		return new DefaultHttpClient(httpParameters);
	}

	private InputStream executeRequest(final HttpRequestBase request) throws ClientProtocolException, IOException, Exception {
		final HttpClient mClient = builHttpClient();

		final HttpResponse mResponse = mClient.execute(request);

		final int mStatusCode = mResponse.getStatusLine().getStatusCode();

		// Log.e(C.DEBUG_TAG, mResponse.getStatusLine().getReasonPhrase());
		//
		// final BufferedReader bReader = new BufferedReader(new
		// InputStreamReader(mResponse.getEntity().getContent(), "iso-8859-1"),
		// 8);
		// final StringBuilder sBuilder = new StringBuilder();
		//
		// String line = null;
		// while ((line = bReader.readLine()) != null) {
		// sBuilder.append(line + "\n");
		// }
		//
		// // mResponse.getEntity().getContent().close();
		// Log.e(C.DEBUG_TAG, sBuilder.toString());

		if ((mStatusCode != HttpStatus.SC_OK) && (mStatusCode != HttpStatus.SC_NOT_ACCEPTABLE) && (mStatusCode != HttpStatus.SC_UNAUTHORIZED)) {
			// throw new Exception(mResponse.getStatusLine().getReasonPhrase());
			return null;
		}
		return mResponse.getEntity().getContent();
	}
}