package mx.uv.registrationresultsuv.util;

import mx.uv.registrationresultsuv.preferences.Preferences_;
import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class AnalitycsUtil {

	private static final String PROPERTY_ID = "UA-51294706-6";

	/**
	 * Enum used to identify the tracker that needs to be used for tracking.
	 * 
	 * A single tracker is usually enough for most purposes. In case you do need
	 * multiple trackers, storing them all in Application object helps ensure
	 * that they are created only once per application instance.
	 */
	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg:
						// roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a
							// company.
	}

	public static synchronized Tracker getTracker(Application application,
			TrackerName trackerId) throws Exception {
		GoogleAnalytics analytics = GoogleAnalytics.getInstance(application);
		return analytics.newTracker(PROPERTY_ID);
	}

	public static synchronized void trackView(Activity activity,
			Preferences_ preferences, String name) {
		try {
			if (preferences.IsAnalitycsAccepted().get()) {
				if (C.BLACKBERRY_10_BUILD) {
					EasyTracker.getInstance().setContext(
							activity.getApplicationContext());
					EasyTracker.getTracker().sendView(name);
				} else if (!isGooglePlayServicesAvailable(activity)) {
					EasyTracker.getInstance().setContext(
							activity.getApplicationContext());
					EasyTracker.getTracker().setAppId(PROPERTY_ID);
					EasyTracker.getTracker().sendView(name);
				} else {
					Tracker tracker = getTracker(activity.getApplication(),
							TrackerName.APP_TRACKER);
					// Set screen name.
					// Where path is a String representing the screen name.
					tracker.setScreenName(name);
					// Send a screen view.
					tracker.send(new HitBuilders.AppViewBuilder().build());
					// Clear the screen name field when we're done.
					tracker.setScreenName(null);
				}
			}
		} catch (Exception ex) {
		}
	}

	public static synchronized void trackEvent(Application application,
			Preferences_ preferences, String category, String action,
			String label) {
		try {
			if (preferences.IsAnalitycsAccepted().get()) {
				if (C.BLACKBERRY_10_BUILD) {
					EasyTracker.getInstance().setContext(
							application.getApplicationContext());
					EasyTracker.getTracker().sendEvent(category, action, label,
							(long) 0);
				} else if (!isGooglePlayServicesAvailable(application
						.getApplicationContext())) {
					EasyTracker.getInstance().setContext(
							application.getApplicationContext());
					EasyTracker.getTracker().setAppId(PROPERTY_ID);
					EasyTracker.getTracker().sendEvent(category, action, label,
							(long) 0);
				} else {
					Tracker tracker = getTracker(application,
							TrackerName.APP_TRACKER);
					trackEvent(tracker, category, action, label);
				}
			}
		} catch (Exception ex) {
		}
	}

	public static synchronized void trackEvent(Activity activity,
			Preferences_ preferences, String category, String action,
			String label) {
		try {
			if (preferences.IsAnalitycsAccepted().get()) {
				if (C.BLACKBERRY_10_BUILD) {
					EasyTracker.getInstance().setContext(
							activity.getApplicationContext());
					EasyTracker.getTracker().sendEvent(category, action, label,
							(long) 0);
				} else if (!isGooglePlayServicesAvailable(activity
						.getApplicationContext())) {
					EasyTracker.getInstance().setContext(
							activity.getApplicationContext());
					EasyTracker.getTracker().setAppId(PROPERTY_ID);
					EasyTracker.getTracker().sendEvent(category, action, label,
							(long) 0);
				} else {
					Tracker tracker = getTracker(activity.getApplication(),
							TrackerName.APP_TRACKER);
					trackEvent(tracker, category, action, label);
				}
			}
		} catch (Exception ex) {
		}
	}

	public static synchronized void trackEvent(Tracker tracker,
			String category, String action, String label) throws Exception {
		tracker.send(new HitBuilders.EventBuilder().setCategory(category)
				.setAction(action).setLabel(label).build());
	}

	private static boolean isGooglePlayServicesAvailable(Context context) {
		int googlePlayServicesAvailableCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(context.getApplicationContext());

		if (googlePlayServicesAvailableCode == ConnectionResult.SUCCESS) {
			return true;
		}
		return false;
	}
}
