/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.util;

import mx.uv.registrationresultsuv.R;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * 
 * @author frank
 */
public class NetworkCheckerUtil {

	public static boolean isThereANetworkConnection(Context context) {

		if (C.BLACKBERRY_10_BUILD) {
			return true;
		}
		boolean HaveConnectedWifi = false;
		boolean HaveConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI")) {
				if (ni.isConnected()) {
					HaveConnectedWifi = true;
				}
			}

			if (ni.getTypeName().equalsIgnoreCase("MOBILE")) {
				if (ni.isConnected()) {
					HaveConnectedMobile = true;
				}
			}
		}
		return HaveConnectedWifi || HaveConnectedMobile;
	}

	public static boolean checkNetwork(final Activity activity) {
		if (NetworkCheckerUtil.isThereANetworkConnection(activity.getApplicationContext())) {
			return true;
		} else {
			Toast.makeText(activity, R.string.ctn_internet_required, Toast.LENGTH_LONG).show();
			return false;
		}
	}
}
