/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.util;

import android.content.Context;
import android.widget.Toast;

/**
 * 
 * @author frank
 */
public class ToastUtil {
	public static void longToast(Context context, CharSequence message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	public static void shortToast(Context context, CharSequence message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public static void longToast(Context context, int message) {
		Toast.makeText(context, message, Toast.LENGTH_LONG).show();
	}

	public static void shortToast(Context context, int message) {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
