package mx.uv.registrationresultsuv.util;

import mx.uv.registrationresultsuv.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class UiUtil {
	public static int getStatusBarHeight(final Activity activity) {
		int result = 0;
		final int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = activity.getResources().getDimensionPixelSize(resourceId);

			if (result == getNavigationBarHeight(activity)) {
				result = 0;
			}
		}
		return result;
	}

	public static int getActionBarHeight(final Activity activity) {
		int result = 0;
		final TypedValue tv = new TypedValue();
		if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
			result = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
		}
		return result;
	}

	public static int getNavigationBarHeight(final Activity activity) {
		int result = 0;
		final int resourceId = activity.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = activity.getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	public static boolean isTranslucentNavigationEnabled(final Activity activity) {
		final int id = activity.getResources().getIdentifier("config_enableTranslucentDecor", "bool", "android");
		if (id == 0) {
			return false;
		} else {
			return activity.getResources().getBoolean(id);
			// enabled = are translucent bars supported on this device
		}
	}

	public static Point getHeaderImageSize(final Activity activity) {
		final DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

		final int widthPixels = metrics.widthPixels;

		final int heighPixels = (int) (widthPixels / 1.54);

		return new Point(widthPixels, heighPixels);
	}

	@SuppressLint("NewApi")
	public static int getDensityDpi(final Activity activity) {
		final DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		return metrics.densityDpi;
	}

	public static int getScreenOrientation(final Activity activity) {
		final Display getOrient = activity.getWindowManager().getDefaultDisplay();
		int orientation = Configuration.ORIENTATION_UNDEFINED;

		if (getOrient.getWidth() > getOrient.getHeight()) {
			orientation = Configuration.ORIENTATION_LANDSCAPE;
		} else {
			orientation = Configuration.ORIENTATION_PORTRAIT;

		}

		return orientation;
	}

	public static Point getScreenSize(Activity activity) {
		Display display = activity.getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		// int widthdp = (int) (width /
		// getResources().getDisplayMetrics().density);
		int height = display.getHeight();
		// int heightdp = (int) (height /
		// getResources().getDisplayMetrics().density);
		return new Point(width, height);
	}

	public static boolean isCloseToOneAspectRatio(final Activity activity) {
		Point screenSize = getScreenSize(activity);
		float aspectRatio = ((float) screenSize.x / (float) screenSize.y);
		if ((aspectRatio > .7) && (aspectRatio < 1.3)) {
			return true;
		}
		return false;
	}

	public static boolean isSmallScreen(final Activity activity) {
		final Configuration config = activity.getResources().getConfiguration();

		if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
			return true;
		} else if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			return true;
		}
		return false;
	}

	public static boolean isTablet(final Activity activity) {

		final Configuration config = activity.getResources().getConfiguration();

		if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
			return false;
		} else if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			return false;
		} else if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
			return true;
		} else if ((config.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
			return true;
		}
		return false;
	}

	public static ActionBar setupActionBar(final SherlockFragmentActivity activity, final boolean upAsHome, boolean isSmallScreen) {
		ActionBar actionBar = null;
		try {

			actionBar = activity.getSupportActionBar();

			if (!isSmallScreen) {
				actionBar.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.generic_gradient_top));
				actionBar.setSplitBackgroundDrawable(activity.getResources().getDrawable(R.drawable.generic_gradient_bottom));
			}
			if (upAsHome) {
				actionBar.setHomeButtonEnabled(true);
				actionBar.setDisplayHomeAsUpEnabled(true);
			}

		} catch (final OutOfMemoryError er) {
			System.gc();
			try {
				actionBar = activity.getSupportActionBar();

				actionBar.setBackgroundDrawable(activity.getResources().getDrawable(R.drawable.generic_gradient_top));
				actionBar.setSplitBackgroundDrawable(activity.getResources().getDrawable(R.drawable.generic_gradient_bottom));
				if (upAsHome) {
					actionBar.setHomeButtonEnabled(true);
					actionBar.setDisplayHomeAsUpEnabled(true);
				}

			} catch (final OutOfMemoryError err) {

			}
		}
		return actionBar;
	}

	public static ActionBar setupActionBarWithHome(final SherlockFragmentActivity activity, boolean isSmallScreen) {
		return setupActionBar(activity, true, isSmallScreen);
	}
}
