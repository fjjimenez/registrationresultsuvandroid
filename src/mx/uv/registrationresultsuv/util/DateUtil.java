/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.util;

import java.util.Calendar;

/**
 * 
 * @author frank
 */
public class DateUtil {
	public static boolean isTimeToShowResults() {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		if ((year == 2014) && (month >= Calendar.JUNE) && ((day > 8)) && (day < 30)) {
			return true;
		}
		return false;
	}

	public static boolean isOver9OfJuly() {
		Calendar cal = Calendar.getInstance();
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);

		if ((year == 2014) && (month >= Calendar.JUNE) && (day > 8)) {
			return true;
		}
		return false;
	}
}
