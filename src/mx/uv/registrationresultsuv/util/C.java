/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.util;

/**
 * 
 * @author frank
 */
public class C {

	public static final boolean DEBUG = true;
	public static final String DEBUG_TAG = "Ingreso UV 2014****";

	public static final boolean BLACKBERRY_10_BUILD = false;

	public static final int MIN_DATABASE_VERSION = 1;// 10

	public static final String PARAM_ITEMS = "PARAM_URL";

	public static final int RATE_APP_MIN_LAUNCHES_UNTIL_PROMPT = 3;
	public static final String BLACKBERRY_WORLD_APP_URL = "http://appworld.blackberry.com/webstore/content/54218898/";

	public static final String BASE_URL = "http://movil.dgaeuv.com/wsresultados/index/";
	public static final String STATIC_BASE_URL = "http://www.uv.mx/apps/hipermedios/moviles/ingresouv/2014/json/";
	public static final String STATIC_STEPS_URL = STATIC_BASE_URL + "steps.json";
	public static final String STATIC_SITES_URL = STATIC_BASE_URL + "sites.json";
	public static final String STATIC_PRIVACY_DECLARATION_URL = "http://www.uv.mx/apps/hipermedios/moviles/ingresouv/2014/declarativadeprivacidad.html";
	public static final String STATIC_DEV_SITE_URL = "http://www.uv.mx/cdam/";
	public static final String STATIC_DEV_MAIL = "desarrollomovil@uv.mx";

	public static final String PREFIX_WEB_URL = "http://www.";

	public static final String CACHE_SECTIONS_BY_DEFAULT = "0,0";
	public static final int TIME_TO_UPDATE_A_SECTION = 24 * 60 * 60 * 1000;

	public static final String QUERY = "QUERY";
	public static final String GOOGLE_MAPS_STATIC_IMAGE_API = "AIzaSyASGvU0nEXUWGwlGdKdVrMYJVMNvFv1N6s";

	public static enum SECTION {
		STEPS, SITES
	}

	public enum LOCATION_TYPE {
		SITE, USER
	}

	public enum FRG_TYPE {
		SITES, RESULTS
	}

	public static enum Status {
		OK, NA, TOO_MANY_RESULTS, ERROR
	}

	public static enum DIALOG_TYPE {
		CONFIRM, INFO, ALERT
	}

}
