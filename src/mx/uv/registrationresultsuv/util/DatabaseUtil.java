package mx.uv.registrationresultsuv.util;

import java.util.Calendar;
import java.util.Vector;

import mx.uv.registrationresultsuv.persistence.Persistence;
import android.content.Context;

import com.mobandme.ada.exceptions.AdaFrameworkException;

public final class DatabaseUtil {

	// Singleton pattern
	private static Vector<String> callers;
	private static Persistence persistence;
	private static boolean inUse;
	private static boolean build = false;

	public static Persistence getInstance(final Context context, String caller) throws AdaFrameworkException, Exception {
		if (callers == null) {
			callers = new Vector<String>();
		}

		callers.add(caller);

		// Log.e(C.DEBUG_TAG, "\t Requesting the DB from: " + caller);
		final Calendar start = Calendar.getInstance();
		if (inUse) {
			// Log.e(C.DEBUG_TAG,
			// "\t Sleeping due to some other proccess using the database");
			for (int i = 0; i < 500; i += 1) {
				Thread.sleep(10);
				if (!inUse && callers.get(0).equals(caller)) {
					break;
				}
				if (callers.get(0) == null) {
					callers.add(caller);
					// Log.e(C.DEBUG_TAG, "\tIts alive");
				}
				if ((i == 498) && !callers.get(0).equals(caller)) {
					// Log.e(C.DEBUG_TAG, "Resseting sleep due to " +
					// callers.get(0) + " using the DB");
					i = 0;
				}
			}
		}

		inUse = true;
		if ((persistence == null) && !build) {
			// Log.e(C.DEBUG_TAG, "\t Building the database");
			build = true;
			persistence = new Persistence(context);
		} else if ((persistence == null) && build) {
			// Log.e(C.DEBUG_TAG, "\t Getting the database");
			persistence = new Persistence(context);
		}

		final Calendar end = Calendar.getInstance();

		long time = end.getTimeInMillis() - start.getTimeInMillis();
		if (time > 4000) {
			// Log.e(C.DEBUG_TAG, "\t Time to get DB: " +
			// String.valueOf(end.getTimeInMillis() - start.getTimeInMillis()) +
			// " milliseconds");
			// Log.e(C.DEBUG_TAG, "\t Some process might not release the DB");
		}

		return persistence;
	}

	public static void releaseDB(String caller) {
		int callerIndex = -1;
		for (String c : callers) {
			if (caller.equals(c)) {
				callerIndex = callers.indexOf(c);
			}
		}

		if (callerIndex != -1) {
			callers.remove(callerIndex);
		}
		// Log.e(C.DEBUG_TAG, "\t Releasing the DB from: " + caller);
		DatabaseUtil.inUse = false;
	}
}
