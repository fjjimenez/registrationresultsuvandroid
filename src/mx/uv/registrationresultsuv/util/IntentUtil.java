package mx.uv.registrationresultsuv.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.model.Button;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.persistence.Persistence;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.widget.Toast;

import com.mobandme.ada.exceptions.AdaFrameworkException;

public class IntentUtil {

	protected static String mCaller = IntentUtil.class.toString();

	public static void openWebUrl(final Activity activity, final String url) {
		final Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		try {
			activity.startActivity(i);
		} catch (final Exception e) {
			e.printStackTrace();
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}
	}

	public static void openGooglePlayForRating(final Activity activity) {

		final Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + activity.getPackageName()));
		try {
			activity.startActivity(i);
		} catch (final Exception e) {
			e.printStackTrace();
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}
	}

	// public static void openGoogleStreetView(final Activity activity, final
	// String streetViewNative, final String streetViewWeb) {
	// Intent intent = null;
	//
	// if (android.os.Build.VERSION.SDK_INT < 14) {
	// if (AppInstalledUtil.isAppInstalled(activity.getApplicationContext(),
	// "com.google.android.street")) {
	// intent = new Intent(android.content.Intent.ACTION_VIEW,
	// Uri.parse(streetViewNative));
	// } else {
	// intent = new Intent(android.content.Intent.ACTION_VIEW,
	// Uri.parse(streetViewWeb));
	// }
	// } else {
	// intent = new Intent(android.content.Intent.ACTION_VIEW,
	// Uri.parse(streetViewWeb));
	// if (AppInstalledUtil.isAppInstalled(activity.getApplicationContext(),
	// "com.google.android.apps.maps")) {
	// intent.setClassName("com.google.android.apps.maps",
	// "com.google.android.maps.MapsActivity");
	// }
	// }
	//
	// if (intent != null) {
	// try {
	// activity.startActivity(intent);
	// } catch (final Exception e) {
	// Toast.makeText(activity, R.string.ctn_service_unavailable,
	// Toast.LENGTH_LONG).show();
	// }
	// }
	// }

	public static void openGoogleMaps(final Activity activity, final String googleMapsWeb) {

		final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(googleMapsWeb));
		if (AppInstalledUtil.isAppInstalled(activity.getApplicationContext(), "com.google.android.apps.maps")) {
			intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
		}

		try {
			activity.startActivity(intent);
		} catch (final Exception e) {
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	public static void addArtistToCalendar(final Activity activity, final Step step) {

		if (step.getStartDate() > 0) {

			Persistence persistence = null;
			ArrayList<Button> buttons = null;
			try {
				persistence = DatabaseUtil.getInstance(activity, mCaller + "addArtistToCalendar()");
				buttons = Persistence.getButtons(persistence, step.getID());
			} catch (AdaFrameworkException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				DatabaseUtil.releaseDB(mCaller + "addArtistToCalendar()");
			}

			Calendar startDate = Calendar.getInstance();
			startDate.setTimeInMillis(step.getStartDate() * 1000);

			Calendar endDate = null;
			if (step.getEndDate() > 0) {
				endDate = Calendar.getInstance();
				endDate.setTimeInMillis(step.getEndDate() * 1000);
			} else {
				endDate = startDate;
			}

			final Calendar calStart = new GregorianCalendar();
			calStart.set(Calendar.YEAR, startDate.get(Calendar.YEAR));
			calStart.set(Calendar.MONTH, startDate.get(Calendar.MONTH));
			calStart.set(Calendar.DAY_OF_MONTH, startDate.get(Calendar.DAY_OF_MONTH));
			calStart.set(Calendar.HOUR_OF_DAY, 9);
			calStart.set(Calendar.MINUTE, 0);
			calStart.set(Calendar.SECOND, 0);

			final Calendar calEnd = new GregorianCalendar();
			calEnd.set(Calendar.YEAR, endDate.get(Calendar.YEAR));
			calEnd.set(Calendar.MONTH, endDate.get(Calendar.MONTH));
			calEnd.set(Calendar.DAY_OF_MONTH, endDate.get(Calendar.DAY_OF_MONTH));
			calEnd.set(Calendar.HOUR_OF_DAY, 18);
			calEnd.set(Calendar.MINUTE, 0);
			calEnd.set(Calendar.SECOND, 0);

			String url = "";
			if ((buttons != null) && (!buttons.isEmpty())) {
				url = "\n\nEnlace: " + buttons.get(0).getUrl();
			}

			String content = step.getContent();
			if (C.BLACKBERRY_10_BUILD) {
				content = "";
			}

			final Intent intent = new Intent(Intent.ACTION_INSERT);
			intent.setData(Events.CONTENT_URI);
			intent.putExtra(Events.CALENDAR_COLOR, activity.getResources().getColor(R.color.ingreso_blue_keppel));
			intent.putExtra(Events.CALENDAR_COLOR_KEY, activity.getResources().getColor(R.color.ingreso_blue_keppel));
			intent.putExtra(Events.DISPLAY_COLOR, activity.getResources().getColor(R.color.ingreso_blue_keppel));
			intent.putExtra(Events.TITLE, step.getTitle());

			intent.putExtra(Events.DESCRIPTION, content + url);

			intent.putExtra(Events.HAS_ALARM, true);
			intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, calStart.getTime().getTime());
			intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, calEnd.getTime().getTime());
			try {
				activity.startActivity(intent);
			} catch (final Exception e) {
				Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}
	}

	public static void shareAddress(final Activity activity, final Site site) throws NotFoundException, UnsupportedEncodingException {

		final Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, activity.getResources().getString(R.string.int_share_address, site.getName() + "\n\n" + site.getAddress(), buildGoogleMapsAddress(site)));
		sendIntent.setType("text/plain");

		try {
			activity.startActivity(Intent.createChooser(sendIntent, activity.getText(R.string.int_share)));
		} catch (final Exception e) {
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}
	}

	@SuppressLint("NewApi")
	public static void shareAddress(final Activity activity, final Site site, final Bitmap bitmap) throws NotFoundException, UnsupportedEncodingException {

		if (bitmap == null) {
			shareAddress(activity, site);
		} else {

			String imageUri = saveToSDCard(activity, bitmap, site.getName());
			if ((imageUri != null) && (imageUri.length() > 0)) {
				if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
					final android.text.ClipboardManager clipboard = (android.text.ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
					clipboard.setText(activity.getResources().getString(R.string.int_share_address, site.getName() + "\n\n" + site.getAddress(), buildGoogleMapsAddress(site)));
				} else {
					final android.content.ClipboardManager clipboard = (android.content.ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
					final android.content.ClipData clip = android.content.ClipData.newPlainText(site.getAddress(), activity.getResources().getString(R.string.int_share_address, site.getName() + "\n\n" + site.getAddress(), buildGoogleMapsAddress(site)));
					clipboard.setPrimaryClip(clip);
				}
				ToastUtil.longToast(activity, activity.getText(R.string.ctn_text_copied));

				final Intent sendIntent = new Intent(Intent.ACTION_SEND);
				final Uri uri = Uri.fromFile(new File(imageUri));

				sendIntent.setType("image/*");
				sendIntent.putExtra(Intent.EXTRA_TEXT, activity.getResources().getString(R.string.int_share_address, site.getName() + "\n\n" + site.getAddress(), buildGoogleMapsAddress(site)));
				sendIntent.putExtra(Intent.EXTRA_STREAM, uri);

				try {
					activity.startActivity(Intent.createChooser(sendIntent, activity.getResources().getText(R.string.int_share)));
				} catch (final Exception e) {
					Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
				}
			} else {
				shareAddress(activity, site);
			}
		}
	}

	public static String buildGoogleMapsAddress(Site site) throws UnsupportedEncodingException {
		return "http://maps.google.com/maps?q=" + site.getLatitude() + "," + site.getLongitude() + "(" + URLEncoder.encode(site.getName(), "UTF-8") + ")";

	}

	// http://maps.googleapis.com/maps/api/streetview?size=1080x1920&location=19.5188752,-96.9174159&sensor=false

	public static String buildStreeViewImageAddress(Site site, int width, int height, int orientation) {
		int size;

		if (height > width) {
			size = height;
		} else {
			size = width;
		}

		return "http://maps.googleapis.com/maps/api/streetview?&key=" + C.GOOGLE_MAPS_STATIC_IMAGE_API + "size=" + size + "x" + size + "&location=" + site.getLatitude() + "," + site.getLongitude() + "&sensor=false";
	}

	// http://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794

	public static String buildGoogleMapsImageAddress(Site site, int width, int height, int orientation) throws UnsupportedEncodingException {
		int size;

		if (height > width) {
			size = height;
		} else {
			size = width;
		}

		return "http://maps.googleapis.com/maps/api/staticmap?&key=" + C.GOOGLE_MAPS_STATIC_IMAGE_API + "&scale=2&zoom=17&size=" + size + "x" + size + "&maptype=roadmap&markers=color:red%7Clabel:" + URLEncoder.encode(site.getName(), "UTF-8") + "%7C" + site.getLatitude() + "," + site.getLongitude();
	}

	public static String saveToSDCard(final Context context, Bitmap bitmap, final String finalName) {
		final StringBuffer createdFile = new StringBuffer();

		final File externalStorageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), finalName);
		if (!externalStorageFile.exists()) {

			// final Bitmap resourceImage =
			// BitmapFactory.decodeResource(context.getResources(), resourceID);

			final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			final byte b[] = bytes.toByteArray();

			try {
				externalStorageFile.createNewFile();
				final OutputStream filoutputStream = new FileOutputStream(externalStorageFile);
				filoutputStream.write(b);
				filoutputStream.flush();
				filoutputStream.close();
				createdFile.append(externalStorageFile.getAbsolutePath());
			} catch (final IOException e) {
				e.printStackTrace();
			}
		} else {
			createdFile.append(externalStorageFile.getAbsolutePath());
		}

		return createdFile.toString();
	}

	public static void sendMail(final Activity activity, final String name, final String mail) {

		final Intent intent = new Intent(Intent.ACTION_SENDTO);
		final String uriText = "mailto:" + Uri.encode(mail) + "?subject=" + Uri.encode(activity.getString(R.string.mail_subject)) + "&body=" + Uri.encode(name);
		final Uri uri = Uri.parse(uriText);
		intent.setData(uri);

		final List<ResolveInfo> resolveInfos = activity.getPackageManager().queryIntentActivities(intent, 0);

		// Emulators may not like this check...
		if (!resolveInfos.isEmpty()) {
			try {
				activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.int_send_mail)));
			} catch (final Exception e) {
				// TODO: handle exception
			}

		} else {

			// Nothing resolves send to, so fallback to send...
			final Intent send = new Intent(Intent.ACTION_SEND);

			send.setType("text/plain");
			send.putExtra(Intent.EXTRA_EMAIL, new String[] { mail });
			send.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.mail_subject));
			send.putExtra(Intent.EXTRA_TEXT, name);

			Intent.createChooser(send, activity.getString(R.string.int_send_mail));
		}
	}

	public static void callPhone(final Activity activity, final String name, final String phone) {
		final Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone.replaceAll("[^0-9|\\+]", "").trim()));
		try {
			activity.startActivity(intent);
		} catch (final Exception e) {
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}

	}

	public static void shareString(final Activity activity, final String content) {
		String shareBody = content;
		Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.getResources().getString(R.string.app_full_name));
		sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);

		try {
			activity.startActivity(Intent.createChooser(sharingIntent, activity.getResources().getString(R.string.share_using)));
		} catch (final Exception e) {
			Toast.makeText(activity, R.string.ctn_service_unavailable, Toast.LENGTH_LONG).show();
		}
	}

}
