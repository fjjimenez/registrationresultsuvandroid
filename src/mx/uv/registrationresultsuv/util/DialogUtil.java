package mx.uv.registrationresultsuv.util;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

public class DialogUtil {
	public static void showAcceptAnalitycsDialog(Activity activity, Preferences_ preferences, OnClickListener onClickListener) {
		new AlertDialog.Builder(activity).setTitle(R.string.tvw_privacy_title).setMessage(R.string.tvw_privacy_detail).setPositiveButton(R.string.btn_analitycs_yes, onClickListener).setNegativeButton(R.string.btn_analitycs_no, onClickListener).setOnCancelListener(null).create().show();
	}

	private static final String getApplicationName(Context context) {
		final PackageManager packageManager = context.getPackageManager();
		ApplicationInfo applicationInfo;
		try {
			applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 0);
		} catch (final NameNotFoundException e) {
			applicationInfo = null;
		}
		return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : "(unknown)");
	}
}
