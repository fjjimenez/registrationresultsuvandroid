package mx.uv.registrationresultsuv.util;

import java.util.Calendar;

import mx.uv.registrationresultsuv.preferences.Preferences_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.EBean.Scope;

import android.util.Log;

@EBean(scope = Scope.Singleton)
public class CacheUtil {

	public static void cacheSection(final Preferences_ preferences, final C.SECTION section) {
		Log.e(C.DEBUG_TAG, "Caching: " + section.toString());

		String[] lastUpdate = preferences.LastUpdate().get().split(",");

		if (lastUpdate.length < 0) {
			lastUpdate = C.CACHE_SECTIONS_BY_DEFAULT.split(",");
		}
		lastUpdate[section.ordinal()] = String.valueOf(Calendar.getInstance().getTimeInMillis());

		final StringBuilder sb = new StringBuilder();
		for (int i = 0; i < lastUpdate.length; i += 1) {
			sb.append(lastUpdate[i]);
			if (i != (lastUpdate.length - 1)) {
				sb.append(",");
			}
		}
		preferences.LastUpdate().put(sb.toString());

	}

	public static long getCacheSection(final Preferences_ preferences, final C.SECTION section) {
		final String[] lastUpdate = preferences.LastUpdate().get().split(",");
		return Long.valueOf(lastUpdate[section.ordinal()]);
	}

	public static void clearCache(final Preferences_ preferences) {
		preferences.LastUpdate().put(C.CACHE_SECTIONS_BY_DEFAULT);
	}

	public static boolean isTimeToUpdate(final Preferences_ preferences, final C.SECTION section) {
		final long lastUpdate = Calendar.getInstance().getTimeInMillis() - getCacheSection(preferences, section);
		if ((lastUpdate >= C.TIME_TO_UPDATE_A_SECTION) || (lastUpdate == 0)) {
			return true;
		}
		return false;
	}
}
