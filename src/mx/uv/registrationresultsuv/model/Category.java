package mx.uv.registrationresultsuv.model;

public class Category {

	protected String mTitle;

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	Category(String title) {
		mTitle = title;
	}
}
