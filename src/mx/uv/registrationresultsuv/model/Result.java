/*
 * To change this template;private String  choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.text.WordUtils;

import com.google.gson.annotations.SerializedName;
import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;

/**
 * 
 * @author frank
 */

@Table(name = "Result")
public class Result extends Entity implements Serializable {

	// {"status":1,
	// "resultados":
	// [{"folio":"311948705",
	// "nombre":"CLAUDIA INES ORTIZ PALMERO""solicitudes":
	// [{"area":"AREA ARTES",
	// "programa":"DISE\u00d1O DE LA COMUNICACI\u00d3N VISUAL",
	// "modalidad":"ESCOLARIZADO",
	// "resultado":"Sin derecho",
	// "region":"XALAPA",
	// "lugar":92},
	// {"area":"AREA ARTES",
	// "programa":"ARTES VISUALES",
	// "modalidad":"ESCOLARIZADO",
	// "resultado":"Sin derecho",
	// "region":"XALAPA",
	// "lugar":129}]}]}

	@SerializedName("area")
	private String area;

	@SerializedName("programa")
	private String career;

	@SerializedName("modalidad")
	private String type;

	@SerializedName("modo")
	private String mode;

	@SerializedName("resultado")
	private String accepted;

	@SerializedName("region")
	private String region;

	@SerializedName("lugar")
	private int place;

	public String getAccepted() {
		return WordUtils.capitalize(accepted.toLowerCase());
	}

	public void setAccepted(String accepted) {
		this.accepted = accepted;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getArea() {
		String wipArea = area;
		if (area.contains("AREA")) {
			wipArea = area.replace("AREA ", "");
		}
		return WordUtils.capitalize(wipArea.toLowerCase());
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCareer() {
		return WordUtils.capitalize(career.toLowerCase());
	}

	public void setCareer(String career) {
		this.career = career;
	}

	public int getPlace() {
		return place;
	}

	public void setPlace(int place) {
		this.place = place;
	}

	public String getRegion() {
		return WordUtils.capitalize(region.toLowerCase());
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getType() {
		return WordUtils.capitalize(type.toLowerCase());
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getShareResultString() {
		if (isAccepted()) {
			return "#SiQuede en " + WordUtils.capitalize(career.toLowerCase()) + ", lugar: " + place + ", con derecho a inscripcion #IngresoUV 2014";
		} else {
			return "Sin derecho a ingresar a " + WordUtils.capitalize(career.toLowerCase()) + ", lugar: " + place + " #IngresoUV 2014";
		}
	}

	public boolean isAccepted() {
		ArrayList<String> modes = new ArrayList<String>();
		modes.add("1a");
		modes.add("1b");
		modes.add("1c");
		modes.add("2a");

		mode = mode.toLowerCase();

		for (String m : modes) {
			if (mode.equals(m)) {
				return true;
			}
		}
		return false;
	}
}
