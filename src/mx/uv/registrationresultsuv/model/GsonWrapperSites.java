package mx.uv.registrationresultsuv.model;

import java.util.ArrayList;

public class GsonWrapperSites {

	protected ArrayList<Site> sites;

	public ArrayList<Site> getSites() {
		return sites;
	}

	public void setSites(ArrayList<Site> sites) {
		this.sites = sites;
	}
}
