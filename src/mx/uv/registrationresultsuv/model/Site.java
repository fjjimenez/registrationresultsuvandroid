package mx.uv.registrationresultsuv.model;

import java.io.Serializable;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.services.SitesRequestService_;
import android.app.Activity;

import com.google.gson.annotations.SerializedName;
import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

@Table(name = "Site")
public class Site extends Entity implements Serializable {

	@TableField(name = "campus", datatype = Entity.DATATYPE_INTEGER, required = true)
	private int campus = -1;

	@TableField(name = "name", datatype = Entity.DATATYPE_STRING, required = true)
	private String name = "";

	@TableField(name = "address", datatype = Entity.DATATYPE_STRING, required = true)
	private String address = "";

	@SerializedName("phone_text")
	@TableField(name = "phoneText", datatype = Entity.DATATYPE_STRING, required = true)
	private String phoneText = "";

	@TableField(name = "phone", datatype = Entity.DATATYPE_STRING, required = true)
	private String phone = "";

	@TableField(name = "latitude", datatype = Entity.DATATYPE_DOUBLE, required = true)
	private double latitude = 0;

	@TableField(name = "longitude", datatype = Entity.DATATYPE_DOUBLE, required = true)
	private double longitude = 0;

	@SerializedName("stree_view_web")
	@TableField(name = "streeViewWeb", datatype = Entity.DATATYPE_STRING, required = true)
	private String streeViewWeb = "";

	@SerializedName("stree_view_native")
	@TableField(name = "streeViewNative", datatype = Entity.DATATYPE_STRING, required = true)
	private String streeViewNative = "";

	@SerializedName("google_maps")
	@TableField(name = "googleMaps", datatype = Entity.DATATYPE_STRING, required = true)
	private String googleMaps = "";

	public int getCampusResourceID() {
		switch (campus) {
		case 0:
			return R.string.campus_xalapa;
		case 1:
			return R.string.campus_veracruz;
		case 2:
			return R.string.campus_orizaba_cordoba;
		case 3:
			return R.string.campus_poza_rica_tuxpan;
		case 4:
			return R.string.campus_coatzacoalcos_minatitlan;
		default:
			return R.string.campus_uvi;
		}
	}

	public String getCampusName(Activity activity) {
		return activity.getString(getCampusResourceID());
	}

	public int getCampus() {
		return campus;
	}

	public void setCampus(int campus) {
		this.campus = campus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneText() {
		return phoneText;
	}

	public void setPhoneText(String phoneText) {
		this.phoneText = phoneText;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getStreeViewWeb() {
		return streeViewWeb;
	}

	public void setStreeViewWeb(String streeViewWeb) {
		this.streeViewWeb = streeViewWeb;
	}

	public String getStreeViewNative() {
		return streeViewNative;
	}

	public void setStreeViewNative(String streeViewNative) {
		this.streeViewNative = streeViewNative;
	}

	public String getGoogleMaps() {
		return googleMaps;
	}

	public void setGoogleMaps(String googleMaps) {
		this.googleMaps = googleMaps;
	}

	public static void requestFromServer(final Activity activity) {
		SitesRequestService_.intent(activity).start();
	}
}
