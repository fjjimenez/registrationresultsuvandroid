package mx.uv.registrationresultsuv.model;

public class Item {

	String mTitle;
	int mIconRes;

	public String getmTitle() {
		return mTitle;
	}

	public void setmTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	public int getmIconRes() {
		return mIconRes;
	}

	public void setmIconRes(int mIconRes) {
		this.mIconRes = mIconRes;
	}

	public Item(String title, int iconRes) {
		mTitle = title;
		mIconRes = iconRes;
	}
}
