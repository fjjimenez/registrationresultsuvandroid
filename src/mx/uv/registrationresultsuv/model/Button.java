package mx.uv.registrationresultsuv.model;

import java.io.Serializable;

import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

@Table(name = "Button")
public class Button extends Entity implements Serializable {

	private static final long serialVersionUID = -577507230785664772L;

	@TableField(name = "stepID", datatype = Entity.DATATYPE_LONG, required = true)
	protected long stepID = -1;

	@TableField(name = "title", datatype = Entity.DATATYPE_STRING, required = true)
	protected String title = "";

	@TableField(name = "icon", datatype = Entity.DATATYPE_STRING, required = true)
	protected String icon = "";

	@TableField(name = "url", datatype = Entity.DATATYPE_STRING, required = true)
	protected String url = "";

	@TableField(name = "action", datatype = Entity.DATATYPE_INTEGER, required = true)
	protected int action = -1;

	public long getStepID() {
		return stepID;
	}

	public void setStepID(long stepID) {
		this.stepID = stepID;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getAction() {
		return action;
	}

	public void setAction(int action) {
		this.action = action;
	}

}
