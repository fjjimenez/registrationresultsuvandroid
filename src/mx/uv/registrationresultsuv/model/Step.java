package mx.uv.registrationresultsuv.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import mx.uv.registrationresultsuv.R;
import mx.uv.registrationresultsuv.services.StepsRequestService_;
import android.app.Activity;

import com.google.gson.annotations.SerializedName;
import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;

@Table(name = "Step")
public class Step extends Entity implements Serializable {

	private static final long serialVersionUID = 2015615347226122349L;

	@TableField(name = "number", datatype = Entity.DATATYPE_INTEGER, required = true)
	protected int number = -1;

	@TableField(name = "title", datatype = Entity.DATATYPE_STRING, required = true)
	protected String title = "";

	@TableField(name = "subtitle", datatype = Entity.DATATYPE_STRING, required = true)
	protected String subtitle = "";

	@SerializedName("start_date")
	@TableField(name = "startDate", datatype = Entity.DATATYPE_LONG, required = true)
	protected long startDate = 0;

	@SerializedName("end_date")
	@TableField(name = "endDate", datatype = Entity.DATATYPE_LONG, required = true)
	protected long endDate = 0;

	@TableField(name = "content", datatype = Entity.DATATYPE_STRING, required = true)
	protected String content = "";

	protected ArrayList<Button> buttons;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}

	public long getStartDate() {
		return startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}

	public long getEndDate() {
		return endDate;
	}

	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public ArrayList<Button> getButtons() {
		return buttons;
	}

	public void setButtons(ArrayList<Button> buttons) {
		this.buttons = buttons;
	}

	public int getResourceId() {
		switch (number) {
		case 1:
			return R.drawable.ic_register;
		case 2:
			return R.drawable.ic_money;
		case 3:
			return R.drawable.ic_picture;
		case 4:
			return R.drawable.ic_id_test;
		case 5:
			return R.drawable.ic_test;
		case 6:
			return R.drawable.ic_results;
		case 7:
			return R.drawable.ic_slipping;
		case 8:
			return R.drawable.ic_vacancies;
		default:
			return R.drawable.ic_inscription;
		}
	}

	public String getFormatedDate() {
		String date = "";

		final DateFormat dateFormat = DateFormat.getDateInstance();
		String start = dateFormat.format(new Date(startDate * 1000));
		String end = dateFormat.format(new Date(endDate * 1000));

		if (startDate != 0) {
			if (endDate != 0) {
				date += start + " - " + end;
			} else {
				date += start;
			}
		} else if (endDate != 0) {
			date += end;
		}

		return date;
	}

	public static void requestFromServer(final Activity activity) {
		StepsRequestService_.intent(activity).start();
	}

}
