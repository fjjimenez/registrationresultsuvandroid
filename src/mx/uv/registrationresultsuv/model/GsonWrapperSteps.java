package mx.uv.registrationresultsuv.model;

import java.util.ArrayList;

public class GsonWrapperSteps {

	protected ArrayList<Step> steps;

	public ArrayList<Step> getSteps() {
		return steps;
	}

	public void setSteps(ArrayList<Step> steps) {
		this.steps = steps;
	}
}
