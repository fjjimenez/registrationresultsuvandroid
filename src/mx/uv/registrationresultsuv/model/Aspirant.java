/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.lang3.text.WordUtils;

import com.google.gson.annotations.SerializedName;
import com.mobandme.ada.Entity;
import com.mobandme.ada.annotations.Table;
import com.mobandme.ada.annotations.TableField;
import com.mobandme.ada.annotations.TableIndex;

/**
 * 
 * @author frank
 */

@Table(name = "Aspirant")
public class Aspirant extends Entity implements Serializable {

	@SerializedName("folio")
	@TableField(name = "folio", datatype = Entity.DATATYPE_TEXT, required = true)
	@TableIndex(name = "inx_aspirant_folio", direction = INDEX_DIRECTION_ASC)
	private String folio;

	@SerializedName("nombre")
	@TableField(name = "name", datatype = Entity.DATATYPE_TEXT, required = true)
	private String name;

	@SerializedName("solicitudes")
	// @TableField(name = "results", datatype = Entity.DATATYPE_ENTITY_LINK,
	// required = true)
	private ArrayList<Result> results = new ArrayList<Result>();

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getName() {
		return WordUtils.capitalize(name.toLowerCase());
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Result> getResults() {
		return results;
	}

	public void setResults(ArrayList<Result> results) {
		this.results = results;
	}
}
