/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.uv.registrationresultsuv.model;

import java.io.Serializable;
import java.util.ArrayList;

import mx.uv.registrationresultsuv.services.FolioRequestService;
import mx.uv.registrationresultsuv.services.SearchRequestService;
import mx.uv.registrationresultsuv.services.SearchRequestService_;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.C.Status;
import android.app.Activity;
import android.content.Intent;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author frank
 */
public class Request implements Serializable {

	@SerializedName("status")
	private int status = 0;

	@SerializedName("resultados")
	private ArrayList<Aspirant> persons = new ArrayList<Aspirant>();

	// private Status status = Status.NA;

	public ArrayList<Aspirant> getPersons() {
		return persons;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setPersons(ArrayList<Aspirant> persons) {
		this.persons = persons;
	}

	public Status getRequestStatus() {

		switch (status) {
		case -1:
			return C.Status.NA;
		case 0:
			return C.Status.ERROR;
		case 1:
			return C.Status.OK;
		case 2:
			return C.Status.TOO_MANY_RESULTS;
		default:
			return C.Status.ERROR;
		}
	}

	public static void callSearchRequestService(Activity activity, String query) {
		Intent intent = new Intent(activity, SearchRequestService.class);
		intent.putExtra(C.QUERY, query);
		activity.startService(intent);
	}

	public static void callFolioRequestService(Activity activity, String query) {
		Intent intent = new Intent(activity, FolioRequestService.class);
		intent.putExtra(C.QUERY, query);
		activity.startService(intent);
	}
}
