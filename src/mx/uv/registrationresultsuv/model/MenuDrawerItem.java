package mx.uv.registrationresultsuv.model;

import java.util.ArrayList;

public class MenuDrawerItem {

	private String title;

	private int position;

	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(final int position) {
		this.position = position;
	}

	public MenuDrawerItem(final String title, final int position) {
		super();
		this.title = title;
		this.position = position;
	}

	public static ArrayList<MenuDrawerItem> getMenuItems(final String[] items) {
		final ArrayList<MenuDrawerItem> menuItems = new ArrayList<MenuDrawerItem>();

		int pos = 0;
		for (final String item : items) {
			menuItems.add(new MenuDrawerItem(item, pos));
			pos += 1;
		}
		return menuItems;
	}
}
