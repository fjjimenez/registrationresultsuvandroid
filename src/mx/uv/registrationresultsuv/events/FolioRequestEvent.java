package mx.uv.registrationresultsuv.events;

import mx.uv.registrationresultsuv.model.Request;

public class FolioRequestEvent {

	private Request request;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public FolioRequestEvent(Request request) {
		super();
		this.request = request;
	}

}
