package mx.uv.registrationresultsuv.events;

public class OpenWebUrlEvent {
	protected String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public OpenWebUrlEvent(String url) {
		super();
		this.url = url;
	}

}
