package mx.uv.registrationresultsuv.events;

import mx.uv.registrationresultsuv.model.Result;

public class ShareResultEvent {

	private Result result;

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public ShareResultEvent(Result result) {
		super();
		this.result = result;
	}

}
