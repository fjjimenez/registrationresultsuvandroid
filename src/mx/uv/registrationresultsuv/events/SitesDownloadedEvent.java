package mx.uv.registrationresultsuv.events;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Site;

public class SitesDownloadedEvent {

	protected boolean downloaded;
	protected ArrayList<Site> sites;

	public boolean isDownloaded() {
		return downloaded;
	}

	public void setDownloaded(boolean downloaded) {
		this.downloaded = downloaded;
	}

	public ArrayList<Site> getSites() {
		return sites;
	}

	public void setSites(ArrayList<Site> sites) {
		this.sites = sites;
	}

	public SitesDownloadedEvent(boolean downloaded, ArrayList<Site> sites) {
		super();
		this.downloaded = downloaded;
		this.sites = sites;
	}

}
