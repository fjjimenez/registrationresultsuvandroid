package mx.uv.registrationresultsuv.events;

import mx.uv.registrationresultsuv.util.C.DIALOG_TYPE;

public class ShowMessageEvent {

	protected String message;

	protected DIALOG_TYPE type;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public DIALOG_TYPE getType() {
		return type;
	}

	public void setType(DIALOG_TYPE type) {
		this.type = type;
	}

	public ShowMessageEvent(String message, DIALOG_TYPE type) {
		super();
		this.message = message;
		this.type = type;
	}

}
