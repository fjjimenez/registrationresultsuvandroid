package mx.uv.registrationresultsuv.events;


public class OpenStepEvent {

	private int index;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public OpenStepEvent(int index) {
		super();
		this.index = index;
	}
}
