package mx.uv.registrationresultsuv.events;

import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.C.LOCATION_TYPE;

import com.google.android.gms.maps.model.LatLng;

public class CenterAtLocationEvent {

	protected C.LOCATION_TYPE type;

	protected LatLng location;

	public C.LOCATION_TYPE getType() {
		return type;
	}

	public void setType(C.LOCATION_TYPE type) {
		this.type = type;
	}

	public LatLng getLocation() {
		return location;
	}

	public void setLocation(LatLng location) {
		this.location = location;
	}

	public CenterAtLocationEvent(LOCATION_TYPE type, LatLng location) {
		super();
		this.type = type;
		this.location = location;
	}

}
