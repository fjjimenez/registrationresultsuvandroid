package mx.uv.registrationresultsuv.events;

import mx.uv.registrationresultsuv.model.Request;

public class SearchRequestEvent {

	private Request request;

	public Request getRequest() {
		return request;
	}

	public void setRequest(Request request) {
		this.request = request;
	}

	public SearchRequestEvent(Request request) {
		super();
		this.request = request;
	}
}
