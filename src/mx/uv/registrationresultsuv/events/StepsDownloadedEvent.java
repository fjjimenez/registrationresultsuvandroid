package mx.uv.registrationresultsuv.events;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Step;

public class StepsDownloadedEvent {

	protected boolean downloaded;
	protected ArrayList<Step> steps;

	public boolean isDownloaded() {
		return downloaded;
	}

	public void setDownloaded(boolean downloaded) {
		this.downloaded = downloaded;
	}

	public ArrayList<Step> getSteps() {
		return steps;
	}

	public void setSteps(ArrayList<Step> steps) {
		this.steps = steps;
	}

	public StepsDownloadedEvent(boolean downloaded, ArrayList<Step> steps) {
		super();
		this.downloaded = downloaded;
		this.steps = steps;
	}

}
