package mx.uv.registrationresultsuv.persistence;

import java.util.ArrayList;

import mx.uv.registrationresultsuv.model.Aspirant;
import mx.uv.registrationresultsuv.model.Button;
import mx.uv.registrationresultsuv.model.Result;
import mx.uv.registrationresultsuv.model.Site;
import mx.uv.registrationresultsuv.model.Step;
import mx.uv.registrationresultsuv.preferences.Preferences_;
import mx.uv.registrationresultsuv.util.C;
import mx.uv.registrationresultsuv.util.CacheUtil;
import android.app.Activity;
import android.content.Context;

import com.mobandme.ada.Entity;
import com.mobandme.ada.ObjectContext;
import com.mobandme.ada.ObjectSet;
import com.mobandme.ada.exceptions.AdaFrameworkException;

public final class Persistence extends ObjectContext {

	public ObjectSet<Step> steps;
	public ObjectSet<Button> buttons;
	public ObjectSet<Site> sites;
	public ObjectSet<Aspirant> aspirants;
	public ObjectSet<Result> results;

	public Persistence(final Context pContext) throws AdaFrameworkException {
		super(pContext);

		if (steps == null) {
			steps = new ObjectSet<Step>(Step.class, this);
		}

		if (buttons == null) {
			buttons = new ObjectSet<Button>(Button.class, this);
		}

		if (sites == null) {
			sites = new ObjectSet<Site>(Site.class, this);
		}

		if (aspirants == null) {
			aspirants = new ObjectSet<Aspirant>(Aspirant.class, this);
		}

		if (results == null) {
			results = new ObjectSet<Result>(Result.class, this);
		}

	}

	public ObjectSet<Step> getSteps() throws AdaFrameworkException {
		if (steps == null) {
			steps = new ObjectSet<Step>(Step.class, this);
		}
		return steps;
	}

	public ObjectSet<Button> getButtons() throws AdaFrameworkException {
		if (buttons == null) {
			buttons = new ObjectSet<Button>(Button.class, this);
		}
		return buttons;
	}

	public ObjectSet<Site> getSites() throws AdaFrameworkException {
		if (sites == null) {
			sites = new ObjectSet<Site>(Site.class, this);
		}
		return sites;
	}

	public ObjectSet<Aspirant> getAspirants() throws AdaFrameworkException {
		if (aspirants == null) {
			aspirants = new ObjectSet<Aspirant>(Aspirant.class, this);
		}
		return aspirants;
	}

	public ObjectSet<Result> getResults() throws AdaFrameworkException {
		if (results == null) {
			results = new ObjectSet<Result>(Result.class, this);
		}
		return results;
	}

	public void saveSteps(ArrayList<Step> steps) throws AdaFrameworkException {
		buttons.fill();
		for (Button button : buttons) {
			button.setStatus(Entity.STATUS_DELETED);
		}
		buttons.save();
		buttons.clear();

		this.steps.fill();
		for (Step step : this.steps) {
			step.setStatus(Entity.STATUS_DELETED);
		}
		this.steps.save();
		this.steps.clear();

		for (Step step : steps) {
			this.steps.add(step);
		}
		this.steps.save();

		for (Step step : steps) {
			for (Button button : step.getButtons()) {
				button.setStepID(step.getID());
				buttons.add(button);
			}
		}
		buttons.save();
	}

	public ArrayList<Step> returnSteps() throws AdaFrameworkException {

		steps.fill();

		return (ArrayList<Step>) steps.clone();
	}

	public ArrayList<Button> returnButtons(long id) throws AdaFrameworkException {

		String where = "stepID = ?";
		String[] whereArgs = { "" };

		whereArgs[0] = String.valueOf(id);
		buttons.fill(where, whereArgs, null);
		return (ArrayList<Button>) buttons.clone();
	}

	public void saveSites(ArrayList<Site> sites) throws AdaFrameworkException {
		this.sites.fill();
		for (Site step : this.sites) {
			step.setStatus(Entity.STATUS_DELETED);
		}
		this.sites.save();
		this.sites.clear();

		for (Site step : sites) {
			this.sites.add(step);
		}
		this.sites.save();
	}

	public ArrayList<Site> returnSites() throws AdaFrameworkException {
		sites.fill();
		return (ArrayList<Site>) sites.clone();
	}

	public Site returnSite(Long id) throws AdaFrameworkException {
		return sites.getElementByID(id);
	}

	public static ArrayList<Step> getSteps(final Activity activity, final Preferences_ preferences, final Persistence persistence, final boolean hasInternet) throws AdaFrameworkException {

		final ArrayList<Step> steps = persistence.returnSteps();
		if (((steps == null) || (steps.isEmpty())) && (hasInternet)) {
			Step.requestFromServer(activity);
		} else if ((steps != null) && (hasInternet)) {
			if (CacheUtil.isTimeToUpdate(preferences, C.SECTION.STEPS)) {
				Step.requestFromServer(activity);
			}
		}
		return steps;
	}

	public static ArrayList<Step> getSteps(final Persistence persistence) throws AdaFrameworkException {
		return persistence.returnSteps();
	}

	public static ArrayList<Button> getButtons(Persistence persistence, long id) throws AdaFrameworkException {
		return persistence.returnButtons(id);
	}

	public static ArrayList<Site> getSites(final Activity activity, final Preferences_ preferences, final Persistence persistence, final boolean hasInternet) throws AdaFrameworkException {

		final ArrayList<Site> steps = persistence.returnSites();
		if (((steps == null) || (steps.isEmpty())) && (hasInternet)) {
			Site.requestFromServer(activity);
		} else if ((steps != null) && (hasInternet)) {
			if (CacheUtil.isTimeToUpdate(preferences, C.SECTION.SITES)) {
				Site.requestFromServer(activity);
			}
		}
		return steps;
	}

	public static ArrayList<Site> getSites(final Persistence persistence) throws AdaFrameworkException {
		return persistence.returnSites();
	}

	public static Site getSite(final Persistence persistence, Long id) throws AdaFrameworkException {
		return persistence.returnSite(id);
	}

}
